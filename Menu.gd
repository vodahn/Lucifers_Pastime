extends VBoxContainer

var screensize
var pos
var mouse_position

var p1_container = null
var p2_container = null

var p1_deck
var p2_deck

var p1_mp = 200
var p2_mp = 200

var turn_count = 0
var summ_count = 0

signal turn_done
signal reset_pre_effect(active_player)
signal pre_effect2(active_player)
signal soul_send(active_player, p1_deck, p2_deck)

var active_player
var cell
var mp_script

var sacrifice_count = 0

var sac_block
var queen_require

var p1_prem_effect = 0
var p2_prem_effect = 0
var p1_res_effect = 0
var p2_res_effect = 0
var p1_res_type = null
var p2_res_type = null
var p1_reg_effect = 0
var p2_reg_effect = 0
var p1_fel_effect = 0
var p2_fel_effect = 0
var p1_fel_capd = null
var p2_fel_capd = null
var p1_fel_capr = null
var p2_fel_capr = null

func _ready():
	self.mouse_filter = 2
	for i in get_children():
		i.visible = false
		i.mouse_filter = 2 #ignore

func _process(delta):
	screensize = get_viewport_rect().size

func show_menu(player, picked_cell):
	var nsz_check = null
	var distance_check
	var material_check
	var p1_dist_check = false
	var p2_dist_check = false
	
	active_player = player
	cell = picked_cell
	
	if turn_count == 1:
		if cell.coordinate.y > 3:
			return
	if turn_count == 2:
		if cell.coordinate.y < 9:
			return
	#mp check will go here
	if active_player != null and active_player.name == "p1":
				mp_script = active_player.get_node("p1mp")
	if active_player != null and active_player.name == "p2":
				mp_script = active_player.get_node("p2mp")

	mouse_position = get_global_mouse_position()
	
	if mouse_position.x > 982:
		mouse_position.x -= 155.5
	if mouse_position.y > 420:
		mouse_position.y -= 171
	
	
	set_global_position(mouse_position)
	self.mouse_filter = 1
	
	for i in get_children():
		if active_player.name == "p1":
			if i.get_index() in p1_deck: 
				i.visible = true
				i.mouse_filter = 1 
		if active_player.name == "p2":
			if i.get_index() in p2_deck: 
				i.visible = true
				i.mouse_filter = 1
		
		if ((active_player.name == "p1" and p1_fel_effect == 2) and p1_fel_capd == i.name):
			var dist_check
			var curr_capr_coor
			for x in p1_container.get_children():
				if p1_fel_capr == x.id:
					curr_capr_coor = x.cur_coor
			dist_check = Sacrifice.e_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dre)
			if dist_check == true:
				Summoning.fel_dist_check(dist_check)
				i.visible = true
				i.mouse_filter = 1
			else:
				dist_check = Sacrifice.o_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dro)
				if dist_check == true:
					Summoning.fel_dist_check(dist_check)
					i.visible = true
					i.mouse_filter = 1
				else:
					Summoning.fel_dist_check(dist_check)
					i.visible = false
					i.mouse_filter = 2
			p1_dist_check = dist_check

		if ((active_player.name == "p2" and p2_fel_effect == 2) and p2_fel_capd == i.name):
			var dist_check
			var curr_capr_coor
			for x in p2_container.get_children():
				if p2_fel_capr == x.id:
					curr_capr_coor = x.cur_coor
			dist_check = Sacrifice.e_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dre)
			if dist_check == true:
				Summoning.fel_dist_check(dist_check)
				i.visible = true
				i.mouse_filter = 1
			else:
				dist_check = Sacrifice.o_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dro)
				if dist_check == true:
					Summoning.fel_dist_check(dist_check)
					i.visible = true
					i.mouse_filter = 1
				else:
					Summoning.fel_dist_check(dist_check)
					i.visible = false
					i.mouse_filter = 2
			p2_dist_check = dist_check

		if i.get_index() == 19:
			i.visible = true

	#buttons will need another variable if piece has ritual requirment to prevent premature button enabling
		if cell.occupying != null:
			i.set_disabled(true)
		if cell.occupying == null and i.disabled == true:
			i.set_disabled(false)
		
		#check if queen has been summoned before first turn can be ended
		if i.get_index() == 18 and (turn_count == 1 or turn_count == 2):
			var queen_check = false
			var container
			if turn_count == 1:
				container = p1_container
			else:
				container = p2_container
			i.mouse_filter = 1
			if container != null:
				for p in container.get_children():
					if  p.piece_type == "Queen":
						queen_check = true
			if queen_check == false:
				i.set_disabled(true)
			i.visible = true
		if (i.get_index() == 18 and 
			(((p1_prem_effect == 1 or p1_prem_effect == 2) and active_player.name == "p1") 
				or ((p2_prem_effect == 1 or p2_prem_effect == 2) and active_player.name == "p2"))):
					i.visible = true
					i.mouse_filter = 1
					i.set_disabled(false)
		
		if queen_require == true and i.get_index() != 16:
			i.set_disabled(true)
		
		#checks that both ressurrection's and regicide's second effect aren't "activated" for active player
		#and if the cell isn't in starting zone and if the piece type is wrong for resurrection 
		if ((
			((active_player.name == "p1" and ((p1_res_effect != 2 or cell.coordinate.y > 3)
												or (p1_res_type == null or p1_res_type != i.name))) or
			(active_player.name == "p2" and ((p2_res_effect != 2 or cell.coordinate.y < 9)
												or (p1_res_type == null or p1_res_type != i.name)))) and

			((active_player.name == "p1" and (p1_reg_effect != 2 or cell.coordinate.y > 3) or 
			(active_player.name == "p2" and (p2_reg_effect != 2 or cell.coordinate.y < 9)))))): 
			#checks piece number limit(of ten for most of them)
			if amm_check(i.name, player) == false:
				i.set_disabled(true)
			#checks if piece can be afforeded with current mp
			if price_check(i.name, player) == false:
				i.set_disabled(true)
			
			#this doesn't account for the specific type of piece captured 
			if ((active_player.name == "p1" and p1_fel_effect != 2 and (p1_fel_capd != "Queen Slime" or p1_dist_check != true)) 
				or (active_player.name == "p2" and p2_fel_effect != 2 and (p2_fel_capd != "Queen Slime" or p2_dist_check != true))):
			#piece specific summoning/material check
				if "Queen Slime" in i.name:
					material_check = Sacrifice.material_check("Queen Slime", active_player, cell)
					if material_check == true:
						i.set_disabled(false)
					else:
						i.set_disabled(true)
			if ((active_player.name == "p1" and p1_fel_effect != 2 and (p1_fel_capd != "Automaton" or p1_dist_check != true))
				or (active_player.name == "p2" and p2_fel_effect != 2 and (p2_fel_capd != "Automaton" or p2_dist_check != true))):
				if "Automaton" in i.name:
					material_check = Sacrifice.material_check("Automaton", active_player, cell)
					if material_check == true:
						i.set_disabled(false)
					else:
						i.set_disabled(true)
			if ((active_player.name == "p1" and p1_fel_effect != 2 and (p1_fel_capd != "Sylph" or p1_dist_check != true))
				or (active_player.name == "p2" and p2_fel_effect != 2 and (p2_fel_capd != "Sylph" or p2_dist_check != true))):
				if "Sylph" in i.name:
					material_check = Sacrifice.material_check("Sylph", active_player, cell)
					if material_check == true:
						i.set_disabled(false)
					else:
						i.set_disabled(true)
			
		#no summoning check for menu, no spell card should affect this
		nsz_check = Nosum.nsz_check(active_player, cell)
		if nsz_check == true:
			i.set_disabled(true)
		
		if Summoning.summ_block == true and i.get_index() != 18:
			i.set_disabled(true)
	
	#sacrifice logic
	if cell.occupying != null and active_player.name in cell.occupying and sac_block != true:
		distance_check = Sacrifice.distance_check(cell.occupying, cell.coordinate)
		if distance_check == true:
			get_child(17).visible = true
			get_child(17).mouse_filter = 1
			get_child(17).set_disabled(false)
		else:
			get_child(17).visible = true
			get_child(17).set_disabled(true)
	else:
		
		get_child(17).mouse_filter = 2
	#next call nosum.nsz_check

func hide_menu():
	self.mouse_filter = 2
	for i in get_children():
		i.visible = false
		i.mouse_filter = 2 #ignore

func receive_p1(given_deck):
	p1_deck = given_deck
func receive_p2(given_deck):
	p2_deck = given_deck
	
func amm_check(button_name, player):
	var amm
	var result
	if "Apprentice" in button_name:
		if player.name == "p1":
			if Apprentice.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Apprentice.p2_amm < 10:
				result = true
			else:
				result = false
	if "Iron Maiden" in button_name:
		if player.name == "p1":
			if IronMaiden.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if IronMaiden.p2_amm < 10:
				result = true
			else:
				result = false
	if "Nekomata" in button_name:
		if player.name == "p1":
			if Nekomata.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Nekomata.p2_amm < 10:
				result = true
			else:
				result = false
	if "Harpy" in button_name:
		if player.name == "p1":
			if Harpy.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Harpy.p2_amm < 10:
				result = true
			else:
				result = false
	if "Redcap" in button_name:
		if player.name == "p1":
			if Redcap.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Redcap.p2_amm < 10:
				result = true
			else:
				result = false
	if "Slime" in button_name:
		if player.name == "p1":
			if Slime.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Slime.p2_amm < 10:
				result = true
			else:
				result = false
	if "Holstaur" in button_name:
		if player.name == "p1":
			if Holstaur.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Holstaur.p2_amm < 10:
				result = true
			else:
				result = false
	if "Red Oni" in button_name:
		if player.name == "p1":
			if RedOni.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if RedOni.p2_amm < 10:
				result = true
			else:
				result = false
	if "Blue Oni" in button_name:
		if player.name == "p1":
			if BlueOni.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if BlueOni.p2_amm < 10:
				result = true
			else:
				result = false
	if "Priestess" in button_name:
		if player.name == "p1":
			if Priestess.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Priestess.p2_amm < 10:
				result = true
			else:
				result = false
	if "Imp" in button_name:
		if player.name == "p1":
			if Imp.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Imp.p2_amm < 10:
				result = true
			else:
				result = false
	if "False Angel" in button_name:
		if player.name == "p1":
			if FalseAngel.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if FalseAngel.p2_amm < 10:
				result = true
			else:
				result = false
	if "Queen Slime" in button_name:
		if player.name == "p1":
			if QueenSlime.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if QueenSlime.p2_amm < 10:
				result = true
			else:
				result = false
	if "Automaton" in button_name:
		if player.name == "p1":
			if Automaton.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Automaton.p2_amm < 10:
				result = true
			else:
				result = false
	if "Sylph" in button_name:
		if player.name == "p1":
			if Sylph.p1_amm < 10:
				result = true
			else:
				result = false
		else:
			if Sylph.p2_amm < 10:
				result = true
			else:
				result = false
	if "Queen" in button_name:
		if player.name == "p1":
			if Queen.p1_amm < 2:
				result = true
			else:
				result = false
		else:
			if Queen.p2_amm < 2:
				result = true
			else:
				result = false
	return result
			
func price_check(button_name, player):
	var result
	var mp
	if player.name == "p1":
		mp = p1_mp
	else:
		mp = p2_mp
	
	if "Apprentice" in button_name:
		if mp >= 2:
			result = true
		else:
			result = false
	if "Iron Maiden" in button_name:
		if mp >= 2:
			result = true
		else:
			result = false
	if "Nekomata" in button_name:
		if mp >= 2:
			result = true
		else:
			result = false
	if "Harpy" in button_name:
		if mp >= 10:
			result = true
		else:
			result = false
	if "Redcap" in button_name:
		if mp >= 10:
			result = true
		else:
			result = false
	if "Slime" in button_name:
		if mp >= 10:
			result = true
		else:
			result = false
	if "Holstaur" in button_name:
		if mp >= 10:
			result = true
		else:
			result = false
	if "Red Oni" in button_name:
		if mp >= 10:
			result = true
		else:
			result = false
	if "Blue Oni" in button_name:
		if mp >= 10:
			result = true
		else:
			result = false
	if "Priestess" in button_name:
		if mp >= 20:
			result = true
		else:
			result = false
	if "Imp" in button_name:
		if mp >= 20:
			result = true
		else:
			result = false
	if "False Angel" in button_name:
		if mp >= 1:
			result = true
		else:
			result = false
	if "Queen Slime" in button_name:
		if mp >= 50:
			result = true
		else:
			result = false
	if "Automaton" in button_name:
		if mp >= 50:
			result = true
		else:
			result = false
	if "Sylph" in button_name:
		if mp >= 50:
			result = true
		else:
			result = false
	if "Queen" in button_name:
		if mp >= 50:
			result = true
		else:
			result = false
	return result

func _on_Apprentice_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Apprentice, active_player, cell)
		end_turn()

func _on_Iron_Maiden_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(IronMaiden, active_player, cell)
		end_turn()

func _on_Ittanmomen_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(IttanMomen, active_player, cell)
		end_turn()

func _on_Nekomata_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Nekomata, active_player, cell)
		end_turn()

func _on_Harpy_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Harpy, active_player, cell)
		end_turn()

func _on_Redcap_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Redcap, active_player, cell) 
		end_turn()

func _on_Slime_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Slime, active_player, cell)
		end_turn()

func _on_Holstaur_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Holstaur, active_player, cell)
		end_turn()

func _on_Red_Oni_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(RedOni, active_player, cell)
		end_turn()
		
func _on_Blue_Oni_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(BlueOni, active_player, cell)
		end_turn()

func _on_Priestess_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Priestess, active_player, cell)
		end_turn()

func _on_Imp_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Imp, active_player, cell)
		end_turn()

func _on_False_Angel_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(FalseAngel, active_player, cell)
		end_turn()
		
func _on_Queen_Slime_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(QueenSlime, active_player, cell)
		end_turn()

func _on_Automaton_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Automaton, active_player, cell)
		end_turn()

func _on_Sylph_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Sylph, active_player, cell)
		end_turn()

func _on_Queen_Button_pressed():
	if cell.occupying == null:
		Summoning.creating_piece(Queen, active_player, cell)
		end_turn()

#this is non-functional since exactly two sacrifices are done per turn, needs to also prevent other actions
func _on_Sacrifice_Button_pressed():
	Sacrifice.get_sacrifice(cell)
	sacrifice_count += 1
	if sacrifice_count == 2:
		sacrifice_count = 0
		Movement.mov_block = false
		Summoning.summ_block = false
		emit_signal("turn_done") #sacrificing shouldn't call end_turn(), it would mess up premonition
		yield()
	else:
		Movement.mov_block = true
		Summoning.summ_block = true

func _on_End_Button_pressed():
	summ_count = 0
	emit_signal("reset_pre_effect", active_player.name)
	emit_signal("turn_done")
	yield()
	
func end_turn():
	var queen_check = false
	var container
	if turn_count == 1:
		container = p1_container
	if turn_count == 2:
		container = p2_container
		
	if turn_count == 1 or turn_count == 2:
		summ_count += 1
		if summ_count == 15:
			for p in container.get_children():
				if  p.piece_type == "Queen":
						queen_check = true
			if queen_check == false:
				queen_require = true
				
		if summ_count == 16:
			summ_count = 0
			queen_require = false
			emit_signal("turn_done")
			yield()
	else:
		if (p1_prem_effect == 1 and active_player.name == "p1") or (p2_prem_effect == 1 and active_player.name == "p2") :
			Summoning.summ_block = true
			emit_signal("pre_effect2", active_player.name)
		else:
			emit_signal("turn_done")
			yield()

func set_turn_count():
	turn_count += 1
	if turn_count < 3:
		sac_block = true
	else:
		sac_block = false

func update_p1_container(new_container):
	p1_container = new_container
func update_p2_container(new_container):
	p2_container = new_container

func update_p1_mp(mp):
	p1_mp = mp
func update_p2_mp(mp):
	p2_mp = mp

func send_soul_info():
	var ac
	if turn_count % 2:
		ac = "p1"
	if turn_count % 2 == 0:
		ac = "p2"
	emit_signal("soul_send", ac, p1_deck, p2_deck)

func get_p1_rest(type):
	p1_res_type = type
func get_p2_rest(type):
	p2_res_type = type


func reset():
	pos = null

	p1_container = null
	p2_container = null

	p1_mp = 200
	p2_mp = 200

	turn_count = 0
	summ_count = 0

	active_player = null
	cell = null
	mp_script = null

	sacrifice_count = 0

	sac_block = null
	queen_require = null

	p1_prem_effect = 0
	p2_prem_effect = 0
	p1_res_effect = 0
	p2_res_effect = 0
	p1_res_type = null
	p2_res_type = null
	p1_reg_effect = 0
	p2_reg_effect = 0
	p1_fel_effect = 0
	p2_fel_effect = 0
	p1_fel_capd = null
	p2_fel_capd = null
	p1_fel_capr = null
	p2_fel_capr = null
