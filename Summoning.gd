extends Node

var p1_perp = Vector2(105, 55) #don't ask where this vector came from
var p2_perp = Vector2(-95.38872,-56.55)
var chosen_perp = p1_perp

var newt

var summ_block

var p1_prem_effect = 0
var p2_prem_effect = 0
var p1_res_effect = 0
var p2_res_effect = 0
var p1_res_piece = null
var p2_res_piece = null
var p1_reg_effect = 0
var p2_reg_effect = 0
var p1_fel_effect = 0
var p2_fel_effect = 0
var p1_fel_capd = null
var p2_fel_capd = null
var p1_fel_capr = null
var p2_fel_capr = null
var fel_dist_bool = false

signal board_call
signal update_p1_container(container)
signal update_p2_container(container)
signal update_p1_mp(mp)
signal update_p2_mp(mp)
signal reset_fel(active_player)

signal mp_display

var post_game = false

func creating_piece(piece_type, active_player, cell): #final might put new in the container based on active_player, not as input
	var mp_script
	if active_player.name ==  "p1":
		var container = active_player.get_node("p1 container")
		piece_type.p1_number[1] += 1
		var new = piece_type.p1_piece.instance()
		mp_script = active_player.get_node("p1mp")
		
		if (((p1_res_effect != 2 or cell.coordinate.y > 3) or (piece_type.name != p1_res_piece)) 
			and (p1_reg_effect != 2 or cell.coordinate.y > 3)):
			if piece_type == FalseAngel:
				mp_script.mp = 0
			else:
				mp_script.mp -= piece_type.cost
		else:
			print("huh")
		
		if piece_type == Queen:
			if Queen.p1_amm == 0:
				mp_script.mp += 50
		
		new.id_setup(piece_type.p1_number)
		new.scale = Vector2(.17, .17)
		new.cur_coor = cell.coordinate
		
		new.connect("move_start", Movement, "movement_begin")
		new.connect("move_end", Movement, "movement_end")
		
		placing_piece(new, container, cell, active_player, mp_script.mp)
		cell.set_occupying(new.id) 
		
		
	if active_player.name == "p2":
		var container = active_player.get_node("p2 container")
		piece_type.p2_number[1] += 1
		var new = piece_type.p2_piece.instance()
		mp_script = active_player.get_node("p2mp")
		
		if (((p2_res_effect != 2 or cell.coordinate.y < 9) or (piece_type.name != p2_res_piece)) 
			and (p2_reg_effect != 2 or cell.coordinate.y < 9)):
			if piece_type == FalseAngel:
				mp_script.mp = 0
			else:
				mp_script.mp -= piece_type.cost
		
		if piece_type == Queen:
			if Queen.p2_amm == 0:
				mp_script.mp += 50
		
		new.id_setup(piece_type.p2_number)
		new.scale = Vector2(.17, .17)
		new.cur_coor = cell.coordinate
		
		new.connect("move_start", Movement, "movement_begin")
		new.connect("move_end", Movement, "movement_end")
		
		placing_piece(new, container, cell, active_player, mp_script.mp)
		cell.set_occupying(new.id) 
		
	Notation.summ_print(piece_type, cell.name)
		
func placing_piece(new_piece, container, cell, active_player, mp):
	new_piece.incr_amm()
	new_piece.global_position = cell.global_position + chosen_perp
	new_piece.z_index = 0
	Nosum.nsz_calc(active_player, cell, new_piece)
	
	if ((active_player.name == "p1" and p1_prem_effect == 1) or (active_player.name == "p2" and p2_prem_effect == 1)):
		Movement.prem_summoned = new_piece.id
	
	if ((active_player.name == "p1" and p1_fel_effect == 2 and new_piece.piece_type == p1_fel_capd) or
		(active_player.name == "p2" and p2_fel_effect == 2 and new_piece.piece_type == p2_fel_capd)):
			if fel_dist_bool == true:
				print("summoning reset send")
				emit_signal("reset_fel", active_player.name)
	
	container.add_child(new_piece)
	
	if "p1" in container.name:
		Movement.update_p1_container(container)
		Sacrifice.update_p1_container(container)
		emit_signal("update_p1_container", container)
		emit_signal("update_p1_mp", mp)
	else:
		Movement.update_p2_container(container)
		Sacrifice.update_p2_container(container)
		emit_signal("update_p2_container", container)
		emit_signal("update_p2_mp", mp)
	
	emit_signal("mp_display")

func block_summ():
	summ_block = true
func unblock_summ():
	summ_block = false

func fel_dist_check(gbool):
	fel_dist_bool = gbool
	
func post_game():
	post_game = true

func change_perp(player):
	if player == "p1":
		chosen_perp = p1_perp
	else:
		chosen_perp = p2_perp

func reset():
	newt = null

	summ_block = null

	p1_prem_effect = 0
	p2_prem_effect = 0
	p1_res_effect = 0
	p2_res_effect = 0
	p1_res_piece = null
	p2_res_piece = null
	p1_reg_effect = 0
	p2_reg_effect = 0
	p1_fel_effect = 0
	p2_fel_effect = 0
	p1_fel_capd = null
	p2_fel_capd = null
	p1_fel_capr = null
	p2_fel_capr = null
	fel_dist_bool = false

	post_game = false
