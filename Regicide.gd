extends Node

func effect(active_player):
	if active_player == "p1":
		Summoning.p1_reg_effect = 1
		Sacrifice.p1_reg_effect = 1
		Notation.p1_reg_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p1_reg_effect = 1
		get_parent().p1_reg_turn = get_parent().turn_count
	else:
		Summoning.p2_reg_effect = 1
		Sacrifice.p2_reg_effect = 1
		Notation.p2_reg_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p2_reg_effect = 1
		get_parent().p2_reg_turn = get_parent().turn_count

func effect2(active_player):
	if active_player == "p1":
		Summoning.p1_reg_effect = 2
		Sacrifice.p1_reg_effect = 2
		Notation.p1_reg_effect = 2
		get_parent().get_parent().get_node("Context Menu/Menu").p1_reg_effect = 2
	else:
		Summoning.p2_reg_effect = 2
		Sacrifice.p2_reg_effect = 2
		Notation.p2_reg_effect = 2
		get_parent().get_parent().get_node("Context Menu/Menu").p2_reg_effect = 2

func effect_reset(active_player):
	if active_player == "p1":
		Summoning.p1_reg_effect = 0
		Sacrifice.p1_reg_effect = 0
		Notation.p1_reg_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p1_reg_effect = 0

	else:
		Summoning.p2_reg_effect = 0
		Sacrifice.p2_reg_effect = 0
		Notation.p2_reg_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p2_reg_effect = 0
