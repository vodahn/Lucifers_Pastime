extends Node

var p1_container = null
var p2_container = null

var one_action

var p1_deck
var p2_deck
var p1_spell
var p2_spell
var p1_sn
var p2_sn
var spell_used = null

var p1_mp = 200
var p2_mp = 200

var turn_count = 0
var summ_count = 0

signal ask_p1
signal ask_p2

signal request_cell(coordinate)
signal request_cell2(coordinate)

signal turn_done
signal reset_pre_effect(active_player)
signal pre_effect2(active_player)

signal send_p1_info(np1_deck)
signal send_p2_info(np2_deck)

signal spell_use
signal ss_use
signal send_output(output)

var active_player
var cell = null
var cell2 = null
var summ_piece
var mp_script

var p1s_usable = true
var p2s_usable = true

var sacrifice_count = 0

var queen_require

var sac_block
var first_turns = true

var p1_prem_effect = 0
var p2_prem_effect = 0
var p1_res_effect = 0
var p2_res_effect = 0
var p1_res_type = null
var p2_res_type = null
var p1_reg_effect = 0
var p2_reg_effect = 0
var p1_fel_effect = 0
var p2_fel_effect = 0
var p1_fel_capd = null
var p2_fel_capd = null
var p1_fel_capr = null
var p2_fel_capr = null

var lett_dict = {"a": "A", "b": "B", "c": "C", "d": "D", "e": "E", "f": "F", "g": "G", "h": "H", 
	"i": "I", "j": "J", "k": "K"}
var numb_array = ["11", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1"]
var piece_dict = {"Au": [Automaton, 14, "Automaton"], "A": [Apprentice, 0, "Apprentice"], 
	"It": [IttanMomen, 2, "IttanMomen"], "I": [IronMaiden, 1, "Iron Maiden"], 
	"N": [Nekomata, 3, "Nekomata"] ,"Hs": [Holstaur, 7, "Holstaur"], "H": [Harpy, 4, "Harpy"], "Sy": [Sylph, 15, "Sylph"]
	, "S": [Slime, 6, "Slime"], "Rc": [Redcap, 5, "Redcap"], "Ro": [RedOni, 8, "Red Oni"], 
	"B": [BlueOni, 9, "Blue Oni"], "P": [Priestess, 10, "Priestess"], "Im": [Imp, 11, "Imp"], 
	"F": [FalseAngel, 12, "False Angel"], "Qs": [QueenSlime, 13, "Queen Slime"], "Q": [Queen, 16, "Queen"]}
var spell_array = ["SRB", "SP", "SR", "SSS", "SRE", "SFB"]

func comm_split(input):
	var comms
	var spell_check = false
	var ma_check = false
	var prem_check = false
	
	if turn_count > 0:
	#even if there is no ",", an array will be made with one member 
		comms = input.split(",")
				
		for i in spell_array:
			if i == comms[0]:
				#check if the chosen spell is that player's actual spell
				if (active_player.name == "p1" and i == p1_spell) or (active_player.name == "p2" and i == p2_spell):
					spell_check = true
					if i == "SRB" or i == "RSE" or i == "SSS":
						ma_check = true

			if comms.size() > 1 and first_turns != true:
				if i == comms[1]:
					return
				#not checked on first turn
				if comms.size() > 2:
					return
				
				if spell_check == true and "SSS" in comms[0] and ":" in comms[1]:
					if ((active_player.name == "p1" and p1s_usable == true) or
						 (active_player.name == "p2" and p2s_usable == true)):
						soul_swap(comms[1])
						return
		
		if comms.size() > 1:
			if (((active_player.name == "p1" and p1_prem_effect == 1) or 
				(active_player.name == "p2" and p2_prem_effect == 1)) and
				("*" in comms[0] and ("-" in comms[1] or "~" in comms[1]))):
					print("success formation")
					ma_check = true
					prem_check = true
		
		if comms.size() == 1:
			one_action = true
		else:
			one_action = false

		if turn_count > 2 and ((spell_check == false and comms.size() > 1 and prem_check == false) or
			  (ma_check == true and comms.size() > 2)):
			print("invalid numer of commands")
			return

		if first_turns == true and comms.size() > 16:
			return
		for i in comms:
			act_split(i, spell_check, ma_check)
		
		#if somebody fails to summon a queen, they should instantly lose  
		if first_turns == true:
			end_turn()

func act_split(comm, sc, mac):
	if "*" in comm:
		summ_logic(comm, sc, mac)

	if first_turns == false:
		if "-" in comm or "~" in comm:
			if "~" in comm and "-" in comm:
				return
			var splitter
			if "-" in comm:
				splitter = "-"
			if "~" in comm:
				splitter = "~"
			var move = comm.split(splitter)
			get_cell(move[0], 1)
			get_cell(move[1], 2)
			var piece = null
			if cell != null and cell2 != null:
				if active_player.name == "p1":
					for i in p1_container.get_children():
						if i.id == cell.occupying:
							piece = i
				else:
					for i in p2_container.get_children():
						if i.id == cell.occupying:
							piece = i
			if piece != null:
				Movement.initial_cell = cell
				Movement.final_cell = cell2
				Movement.moving_piece = piece
				#movement ends turn on its own
				Movement.valid_check()
				cell = null
				cell2 = null
			else:
				cell = null
				cell2 = null
			return
		
		if "'" in comm:
			var distance_check
			var sac = comm.split(" ")
			var piece = null
			var piece2 = null
			get_cell(sac[0], 1)
			get_cell(sac[1], 2)
			if cell != null and cell2 != null:
				if active_player.name == "p1":
					for i in p1_container.get_children():
						if i.id == cell.occupying:
							piece = i
						if i.id == cell2.occupying and (piece == null or i.id != piece.id):
							piece2 = i
				if active_player.name == "p2":
					for i in p2_container.get_children():
						if i.id == cell.occupying:
							piece = i
						if i.id == cell2.occupying and (piece == null or i.id != piece.id):
							piece2 = i
			if piece != null and piece2 != null and sac_block != true:
				distance_check = Sacrifice.n_distance_check(cell.coordinate, cell2.coordinate)
				if distance_check == true:
					Sacrifice.sacrifice(piece, cell)
					Sacrifice.sacrifice(piece2, cell2)
					cell = null
					cell2 = null
					yield(get_tree().create_timer(.1), "timeout")
					end_turn()
			return
		
		if sc == true:
			if ((active_player.name == "p1" and p1s_usable == true) or
				(active_player.name == "p2" and p2s_usable == true)):
					if active_player.name == "p1":
						spell_used = p1_spell
					else:
						spell_used = p2_spell
					emit_signal("spell_use")

func nspell_use():
	if active_player.name == "p1":
		spell_used = p1_spell
	else:
		spell_used = p2_spell
func nspell_disable():
	if active_player.name == "p1":
		p1s_usable = false
	else:
		p2s_usable = false

func summ_logic(comm, sc, mac):
		var pt = null
		var deck 
		var p1_dist_check = false
		var p2_dist_check = false
		if active_player.name == "p1":
			deck = p1_deck
		else:
			deck = p2_deck
		get_cell(comm, 1)
		

		if cell != null and cell.occupying == null:
			if turn_count == 1:
				if cell.coordinate.y > 3:
					return
			if turn_count == 2:
				if cell.coordinate.y < 9:
					return 

			for key in piece_dict:
				if key in comm:
					pt = piece_dict[key]
					break
			if pt != null:
				var deck_check = false
				for i in deck:
					if i == pt[1]:
						deck_check = true
				if ((deck_check == true) or 
					(((active_player.name == "p1" and p1_fel_effect == 2) and p1_fel_capd == pt[2]) or
					((active_player.name == "p2" and p2_fel_effect == 2) and p2_fel_capd == pt[2]))):
					#this fails in fel
					var mp_check = true
					var amm_check = true
					var mat_check = true

				#field of blood stuff here to potentially set deck_check to true also
					if ((active_player.name == "p1" and p1_fel_effect == 2) and p1_fel_capd == pt[2]):
						var dist_check
						var curr_capr_coor
						for x in p1_container.get_children():
							if p1_fel_capr == x.id:
								curr_capr_coor = x.cur_coor
						dist_check = Sacrifice.e_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dre)
						if dist_check == true:
							Summoning.fel_dist_check(dist_check)
							deck_check = true
						else:
							dist_check = Sacrifice.o_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dro)
							if dist_check == true:
								Summoning.fel_dist_check(dist_check)
								deck_check = true
							else:
								Summoning.fel_dist_check(dist_check)
								deck_check = false
						p1_dist_check = dist_check

					if ((active_player.name == "p2" and p2_fel_effect == 2) and p2_fel_capd == pt[2]):
						var dist_check
						var curr_capr_coor
						for x in p2_container.get_children():
							if p2_fel_capr == x.id:
								curr_capr_coor = x.cur_coor
						dist_check = Sacrifice.e_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dre)
						if dist_check == true:
							Summoning.fel_dist_check(dist_check)
							deck_check = true
						else:
							dist_check = Sacrifice.o_dist_check(cell.coordinate, curr_capr_coor, Sacrifice.fel_dro)
							if dist_check == true:
								Summoning.fel_dist_check(dist_check)
								deck_check = true
							else:
								Summoning.fel_dist_check(dist_check)
								deck_check = false
						p2_dist_check = dist_check
					
					var nsz_check = Nosum.nsz_check(active_player, cell)
					
					if ((((active_player.name == "p1" and ((p1_res_effect != 2 or cell.coordinate.y > 3)
												or (p1_res_type == null or (p1_res_type != pt[0].name and p1_res_type != pt[2]) ))) or
						(active_player.name == "p2" and ((p2_res_effect != 2 or cell.coordinate.y < 9)
												or (p2_res_type == null or (p2_res_type != pt[0].name and p2_res_type != pt[2]) )))) and

						((active_player.name == "p1" and (p1_reg_effect != 2 or cell.coordinate.y > 3) or 
						(active_player.name == "p2" and (p2_reg_effect != 2 or cell.coordinate.y < 9)))))): 

							mp_check = mp_check(pt[0])
							amm_check = amm_check(pt[0])
							
							#another fel effect statement here
							if ((active_player.name == "p1" and p1_fel_effect != 2 and (p1_fel_capd != pt[2] or p1_dist_check != true)) 
								or (active_player.name == "p2" and p2_fel_effect != 2 and (p2_fel_capd != pt[2] or p2_dist_check != true))):
			#piece specific summoning/material check
									if pt[0] == Automaton or pt[0] == QueenSlime or pt[0] == Sylph:
										mat_check = Sacrifice.material_check(pt[0].piece_type, active_player, cell)

					if mp_check == true and amm_check == true and mat_check == true and nsz_check != true:
						Summoning.creating_piece(pt[0], active_player, cell)
						if first_turns == false:

							if (p1_prem_effect == 1 and active_player.name == "p1") or (p2_prem_effect == 1 and 
								active_player.name == "p2"):
									if one_action == false:
										Summoning.summ_block = true
										emit_signal("pre_effect2", active_player.name)
									else:
										emit_signal("reset_pre_effect", active_player.name)
										end_turn()
							else:
								end_turn()
					else:

						var error = "invalid summon: " + comm
						print(error)
				else:
					var error = "invalid summon: " + comm
					print(error)
		else:
			var error = "invalid summon: " + comm
			print(error)
		return

func get_cell(comm, number):
	var fcomm = comm
	#filtration 
	if comm.length() > 2:
		if " " in comm:
			var mcomm = comm.split(" ")
			fcomm = mcomm[1]
		if "*" in comm:
			var mcomm = comm.split("*")
			fcomm = mcomm[1] 
		if "'" in comm:
			var mcomm = comm.split("'")
			fcomm = mcomm[1]

	var built_cell = null
	for key in lett_dict:
		if key in fcomm:
			built_cell = lett_dict[key]
			break
	if built_cell != null:
		for i in numb_array:
			if i in fcomm:
				built_cell += i
				break
	if number == 1:
		emit_signal("request_cell", built_cell)
	if number == 2:
		emit_signal("request_cell2", built_cell)

func get_ncell(coor):
	var built_ncell = null
	for key in lett_dict:
		if lett_dict[key] in coor:
			built_ncell = key
			break
	if built_ncell != null:
		for i in numb_array:
			if i in coor:
				built_ncell += i
				return built_ncell
				break
	

func mp_check(piece_type):
	var result
	var mp

	if active_player.name == "p1":
		mp = p1_mp
	else:
		mp = p2_mp

	if first_turns == true and piece_type == Queen:
		var queen_check = false
		var container
		if turn_count == 1:
			container = p1_container
		else:
			container = p2_container
		if container != null:
			for p in container.get_children():
				if  p.piece_type == "Queen":
					queen_check = true
		if queen_check == false:
			result = true

	if result != true:
		if piece_type.cost > mp:
			result = false
		else:
			result = true
	
	return result 

func amm_check(piece_type):
	var result
	var limit

	if piece_type == Queen:
		limit = 2
	else:
		limit = 10

	if active_player.name == "p1":
		if piece_type.p1_amm < limit:
			result = true
		else:
			result = false
	else:
		if piece_type.p2_amm < limit:
			result = true
		else:
			result = false
	return result

func receive_cell(given_cell):
	cell = given_cell
func receive_cell2(given_cell):
	cell2 = given_cell

func soul_swap(swap):
	var pt1
	var pt2
	var ptn1
	var ptn2
	var deck1
	var deck2
	var ocontainer
	var pieces = swap.split(":")
	
	#assigning variables
	if active_player.name == "p1":
		deck1 = p1_deck
		deck2 = p2_deck
		ocontainer = p2_container
	else:
		deck1 = p2_deck
		deck2 = p1_deck
		ocontainer = p1_container
	
	#checking for valid piece types
	for key in piece_dict:
		if key in pieces[0]:
			pt1 = piece_dict[key]
			ptn1 = key
			break
	for key in piece_dict:
		if key in pieces[1]:
			pt2 = piece_dict[key]
			ptn2 = key
			break
	
	#checking if piece types are in players' decks
	#also checking if piece types aren't in other player's deck (duplication)
	if pt1 != null and pt2 != null:
		var deck_check1 = false
		var deck_check2 = false

		for i in deck1:
			if i == pt2[1]:
				return
			if i == pt1[1]:
				deck_check1 = true
		for i in deck2:
			if i == pt1[1]:
				return
			if i == pt2[1]:
				deck_check2 = true


		
		#checking equivalent value
		if deck_check1 == true and deck_check2 == true:
			if pt1[0].cost == pt2[0].cost:
				var piece_check = false
				for p in ocontainer.get_children():
					#here
					if p.piece_type == pt2[2]:
						piece_check = true
				
				
				#performing action
				if piece_check == true:
					for j in deck1:
						if j == pt1[1]:
							deck1.erase(j)
					deck1.append(pt2[1])
					for j in deck2:
						if j == pt2[1]:
							deck2.erase(j)
					deck2.append(pt1[1])

					emit_signal("ss_use")
					ss_print(ptn1, ptn2)
					send_new_decks()
					end_turn()
				else:
					print("no instance of other player's piece")
			else:
				print("piece values are not equal")
		else:
			print("piece(s) not in deck")

func send_new_decks():
	emit_signal("send_p1_info", p1_deck)
	emit_signal("send_p2_info", p2_deck)

func set_turn_count():
	turn_count += 1
	if turn_count >=  3:
		sac_block = false
		first_turns = false
	else:
		sac_block = true
	if turn_count % 2:
		emit_signal("ask_p1")
	if turn_count % 2 == 0:
		emit_signal("ask_p2")

func update_p1_container(new_container):
	p1_container = new_container
func update_p2_container(new_container):
	p2_container = new_container

func update_p1_mp(mp):
	p1_mp = mp
func update_p2_mp(mp):
	p2_mp = mp

func end_turn():
	emit_signal("turn_done")
	yield()

func assign_player(player):
	active_player = player

func receive_p1(given_deck):
	p1_deck = given_deck
func receive_p2(given_deck):
	p2_deck = given_deck

func receive_p1_spell(spell_number):
	p1_spell = numb_to_node(spell_number, "p1")
func receive_p2_spell(spell_number):
	p2_spell = numb_to_node(spell_number, "p2")
	
func numb_to_node(sn, player): #passive effects are activated here
	if player == "p1":
		p1_sn = sn
	else:
		p2_sn = sn
	if sn == 1:
		return "SRB"
	elif sn == 2:
		return "SP"
	elif sn == 3:
		return "SR"
	elif sn == 4:
		return "SSS"
	elif sn == 5:
		return "SRE"
	elif sn == 6:
		return "SFB"

#printing functions send something back to input.gd
func summ_print(piece_type, cell_name):
	var ncoor = get_ncell(cell_name)
	var npiece = null
	for key in piece_dict:
		if piece_dict[key][0] == piece_type:
			npiece = key
			break
	if ncoor != null and npiece != null:
		var output = npiece + "*" +ncoor
		emit_signal("send_output", output) 

func mov_print(piece_type, initial_name, final_name, cap_stat):
	var ninitial = get_ncell(initial_name)
	var nfinal = get_ncell(final_name)
	var npiece = null
	for key in piece_dict:
		#here
		if piece_dict[key][2] == piece_type:
			npiece = key
			break
	if ninitial != null and nfinal != null and npiece != null:
		var output
		if cap_stat == false:
			output = npiece + " " +ninitial + "-" + nfinal
		else:
			output = npiece + " " +ninitial + "~" + nfinal
		emit_signal("send_output", output)

func sac_print(piece_type, site_name):
	var ncoor = get_ncell(site_name)
	var npiece = null
	for key in piece_dict:
		#here
		if piece_dict[key][2] == piece_type:
			npiece = key
			break
	if ncoor != null and npiece != null:
		var output = npiece + "'" + ncoor + " "
		emit_signal("send_output", output) 

func spell_print():
	emit_signal("send_output", spell_used)

func ss_print(ptn1, ptn2):
	var output = ptn1  + " : " + ptn2
	emit_signal("send_output", output) 
func diass_convert(dn1, dn2):
	var ptn1
	var ptn2
	for key in piece_dict:
		if dn1 == piece_dict[key][1]:
			ptn1 = key
		if dn2 == piece_dict[key][1]:
			ptn2 = key
	ss_print(ptn1, ptn2)

func deck_print(deck, player):
	var tdeck
	if player == "p1":
		tdeck = [deck, p1_sn]
	else:
		tdeck = [deck, p2_sn]
	var out_deck1 = str(tdeck).replace("[", "(")
	var out_deck2 = out_deck1.replace("]", ")")
	var output = player + ": " + out_deck2 
	emit_signal("send_output", output) 
	
func get_p1_rest(type):
	p1_res_type = type
func get_p2_rest(type):
	p2_res_type = type

func reset():
	p1_container = null
	p2_container = null

	spell_used = null

	p1_mp = 200
	p2_mp = 200

	turn_count = 0
	summ_count = 0

	active_player = null
	cell = null
	cell2 = null
	summ_piece = null
	mp_script = null

	p1s_usable = true
	p2s_usable = true

	sacrifice_count = 0

	queen_require = null

	sac_block = null
	first_turns = true

	p1_prem_effect = 0
	p2_prem_effect = 0
	p1_res_effect = 0
	p2_res_effect = 0
	p1_res_type = null
	p2_res_type = null
	p1_reg_effect = 0
	p2_reg_effect = 0
	p1_fel_effect = 0
	p2_fel_effect = 0
	p1_fel_capd = null
	p2_fel_capd = null
	p1_fel_capr = null
	p2_fel_capr = null
