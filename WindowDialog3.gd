extends WindowDialog

var p1_deck = []
var p2_deck = []
var p1_spell = null
var p2_spell = null

var piece_dict = {"Au": [Automaton, 14, "Automaton"], "A": [Apprentice, 0, "Apprentice"], 
	"It": [IttanMomen, 2, "IttanMomen"], "I": [IronMaiden, 1, "Iron Maiden"], 
	"N": [Nekomata, 3, "Nekomata"] ,"Hs": [Holstaur, 7, "Holstaur"], "H": [Harpy, 4, "Harpy"], "Sy": [Sylph, 15, "Sylph"]
	, "S": [Slime, 6, "Slime"], "Rc": [Redcap, 5, "Redcap"], "Ro": [RedOni, 8, "Red Oni"], 
	"B": [BlueOni, 9, "Blue Oni"], "P": [Priestess, 10, "Priestess"], "Im": [Imp, 11, "Imp"], 
	"F": [FalseAngel, 12, "False Angel"], "Qs": [QueenSlime, 13, "Queen Slime"], "Q": [Queen, 16, "Queen"]}
var spell_dict = {"SRB": 1, "SP": 2, "SR": 3, "SSS": 4, "SRE": 5, "SFB": 6}

func _ready(): 
	set_process_input(true)
	set_process(true)
	
func _process(_delta):
	if self.visible == true:
		if Input.is_action_pressed("ui_right") == true:
			get_node("DCard Container").set_h_scroll(get_node("DCard Container").get_h_scroll() + 20)
		if Input.is_action_pressed("ui_left") == true:
			get_node("DCard Container").set_h_scroll(get_node("DCard Container").get_h_scroll() - 20)

func _input(event):
	if event.is_action_released("ui_tab"):
		if self.visible == false:
			self.popup_centered()
			if get_parent().get_node("WindowDialog3/p1 button").pressed == true:
				deck_select("p1")
			elif get_parent().get_node("WindowDialog3/p2 button").pressed == true:
				deck_select("p2")
			else:
				get_parent().get_node("WindowDialog3/p1 button").pressed = true
				deck_select("p1")
		else:
			self.visible = false
			get_node("DCard Container").set_h_scroll(0)


func receive_p1(given_deck):
	p1_deck = given_deck
func receive_p2(given_deck):
	p2_deck = given_deck

func receive_p1_spell(spell_number):
	p1_spell = spell_number
func receive_p2_spell(spell_number):
	p2_spell = spell_number

func _on_p1_button_pressed():
	if get_parent().get_node("WindowDialog3/p2 button").pressed == true:
			get_node("DCard Container").set_h_scroll(0)
			get_parent().get_node("WindowDialog3/p2 button").pressed = false
	if get_parent().get_node("WindowDialog3/p1 button").pressed == false:
		get_parent().get_node("WindowDialog3/p1 button").pressed = true
	deck_select("p1")


func _on_p2_button_pressed():
	if get_parent().get_node("WindowDialog3/p1 button").pressed == true:
			get_node("DCard Container").set_h_scroll(0)
			get_parent().get_node("WindowDialog3/p1 button").pressed = false
	if get_parent().get_node("WindowDialog3/p2 button").pressed == false:
		get_parent().get_node("WindowDialog3/p2 button").pressed = true
	deck_select("p2")

func deck_select(player):
	var viewing_deck
	var viewing_spell
	
	for i in get_node("DCard Container/HBoxContainer").get_children():
		i.visible = false	

	if player == "p1":
		viewing_deck = p1_deck
		viewing_spell = p1_spell
	else:
		viewing_deck = p2_deck
		viewing_spell = p2_spell
	
	if viewing_deck == [] or viewing_spell == null:
		return
	
	for i in viewing_deck:
		for key in piece_dict:
			if piece_dict[key][1] == i:
				for s in get_node("DCard Container/HBoxContainer").get_children():
					if piece_dict[key][2] in s.name:
						s.visible = true
	for key in spell_dict:
		if spell_dict[key] == viewing_spell:
			for s in get_node("DCard Container/HBoxContainer").get_children():
				if key in s.name:
					s.visible = true
	get_node("DCard Container/HBoxContainer").get_node("godot margins are shit and need to be fixed but at least I found a workaround").visible = true
	get_node("DCard Container/HBoxContainer").get_node("godot margins are shit and need to be fixed but at least I found a workaround2").visible = true
