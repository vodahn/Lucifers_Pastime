extends Node2D

var mode = "p1"
var mode_setv = false

var store_nsf = []
var store_begin = []

var turn_count = 0

var show_mov = false
var disabled = false

signal send_cell(cell)
signal send_cell2(cell)
signal disable_board

func _ready():
	for i in get_children():
		i.modulate.a = 0.8
		i.get_children()[0].playing = false
	emit_signal("disable_board")
		
func nsz_apply(nsz_cells):
	for i in get_children():
		for x in nsz_cells:
			for y in x:
				for z in y[0]:
					if i.coordinate == z:
						if i.anim != "mov":
							i.anim = "nosum"
						else:
							store_nsf.append(i)
	
	
func field_hide():
	store_nsf = []
	for i in get_children():
		if i.anim != "mov" and i.anim != "begin":
			i.anim = "default"
	if Summoning.post_game == false:
		apply_begin()

func mov_show(trys):
	show_mov = true
	for i in get_children():
		if i.anim == "nosum":
			store_nsf.append(i)
		if i.anim == "begin":
			store_begin.append(i)
		for x in trys:
			if i.coordinate == x:
				i.anim = "mov"
				i.get_children()[0].stop()

func mov_hide():
	if show_mov != true:
		return
	show_mov = false
	for i in get_children():
		if i.anim != "nosum":
			i.anim = "default"
		for x in store_nsf:
			if x == i:
				i.anim = "nosum"
		for x in store_begin:
			if x == i:
				i.anim = "begin"

func set_turn_count():
	turn_count += 1

	if turn_count == 2:
		for i in get_children():
			if i.coordinate.y >= 9:
				i.anim = "begin"
			if i.coordinate.y <= 3 and i.anim != "nosum":
				i.anim = "default"
	if turn_count == 3:
		for i in get_children():
			if i.coordinate.y >= 9 and i.anim != "nosum":
				i.anim = "default"
			
func apply_begin():
	if mode_setv == false:
		return
	if turn_count == 1:
		for i in get_children():
			if i.coordinate.y <= 3:
				i.anim = "begin"
	if turn_count == 2:
		for i in get_children():
			if i.coordinate.y >= 9:
				i.anim = "begin"

func send_cell(coordinate):
	var return_value
	for i in get_children():
		if coordinate == i.name:
			return_value = i
			break
		else:
			return_value = null
	emit_signal("send_cell", return_value)

func send_cell2(coordinate):
	var return_value
	for i in get_children():
		if coordinate == i.name:
			return_value = i
			break
		else:
			return_value = null
	
	emit_signal("send_cell2", return_value)
			
func disable_board():
	for i in get_children():
		i.anim = "default"
	emit_signal("disable_board")

func change_mode(player):
	if player == "p1":
		self.mode = "p1"
		self.rotate(-PI)
		self.position -= Vector2(1762.856, 665.202)
	if player == "p2":
		self.mode = "p2"
		self.rotate(PI)
		self.position += Vector2(1762.856, 665.202)

func mode_set():
	for i in get_children():
		if i.coordinate.y <= 3:
			i.anim = "begin"
	mode_setv = true

func reset():
	mode_setv = false
	
	store_nsf = []
	store_begin = []

	turn_count = 0

	show_mov = false
	disabled = false
	#removing occupying value in all cells
	for i in get_children():
		i.occupying = null
		i.anim = "default"
