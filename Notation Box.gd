extends RichTextLabel


func _ready():
	self.scroll_following = true
	self.get_v_scroll().modulate.a = 0.0
	set_process(true)

func _on_Notation_Box_mouse_exited():
	self.release_focus()

func _on_Notation_Box_meta_clicked(meta):
	var out_meta = meta
	if "(" in meta:
		var mid_meta = meta.replace("(", "[")
		out_meta = mid_meta.replace(")", "]")
	OS.set_clipboard(out_meta)

func _process(_delta):
	if get_global_mouse_position().x > 490 or get_global_mouse_position().x < 420:
		self.get_v_scroll().modulate.a = 0.0
	else:
		self.get_v_scroll().modulate.a = 0.2


