extends Node

signal mouse_control(player)

func play_turn(): #this will include all your options during a turn
	emit_signal("mouse_control", self)
	#Summoning.creating_piece(RedOni, get_node("p2 container")) 

func on_turn(active_player):
	if active_player == self:
		play_turn()
