extends "res://Cell.gd"
var anim = "default"
onready var sprite = self.get_node("Sprite")

var coordinate = Vector2(7,10)

func _ready():
	set_physics_process(true)
	
func _physics_process(delta):
	sprite.play(anim)
	
func _on_Cell_mouse_entered(): 
	if anim != "mov" and anim != "nosum" and anim != "begin":
		anim = "hover"
func _on_Cell_mouse_exited(): 
	if anim != "mov" and  anim != "nosum" and anim != "begin":
		anim = "default"

var occupying #either contains info(vector with piece type, player and id), or is empty
func set_occupying(id): 
	occupying = id
