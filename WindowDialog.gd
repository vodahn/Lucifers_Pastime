extends WindowDialog

var p1_deck = []
var p2_deck = []
var pre_deck = []
var pre_spell = null

var being_chosen

signal p1_ready
signal p2_ready
signal send_p1(deck)
signal send_p2(deck)
signal send_p1_spell(spell)
signal send_p2_spell(spell)

func _ready():
	popup_exclusive = true
	for i in get_children():
		if "Sprite" in i.name:
			i.visible = false

func editor_open():
	self.popup_centered()

func add_spell(input):
	pre_spell = input

func add_to_deck(input):
	if input == "Apprentice":
		pre_deck.append(0)
	if input == "Iron Maiden":
		pre_deck.append(1)
	if input == "IttanMomen":
		pre_deck.append(2)
	if input == "Nekomata":
		pre_deck.append(3)
	if input == "Harpy":
		pre_deck.append(4)
	if input == "Redcap":
		pre_deck.append(5)
	if input == "Slime":
		pre_deck.append(6)
	if input == "Holstaur":
		pre_deck.append(7)
	if input == "Red Oni":
		pre_deck.append(8)
	if input == "Blue Oni":
		pre_deck.append(9)
	if input == "Priestess":
		pre_deck.append(10)
	if input == "Imp":
		pre_deck.append(11)
	if input == "False Angel":
		pre_deck.append(12)
	if input == "Queen Slime":
		pre_deck.append(13)
	if input == "Automaton":
		pre_deck.append(14)
	if input == "Sylph":
		pre_deck.append(15)
	if input == "Queen":
		pre_deck.append(16)

func remove_from_deck(input):
	if input == "Apprentice":
		for i in pre_deck:
			if i == 0:
				pre_deck.erase(i)
	if input == "Iron Maiden":
		for i in pre_deck:
			if i == 1:
				pre_deck.erase(i)
	if input == "IttanMomen":
		for i in pre_deck:
			if i == 2:
				pre_deck.erase(i)
	if input == "Nekomata":
		for i in pre_deck:
			if i == 3:
				pre_deck.erase(i)
	if input == "Harpy":
		for i in pre_deck:
			if i == 4:
				pre_deck.erase(i)
	if input == "Redcap":
		for i in pre_deck:
			if i == 5:
				pre_deck.erase(i)
	if input == "Slime":
		for i in pre_deck:
			if i == 6:
				pre_deck.erase(i)
	if input == "Holstaur":
		for i in pre_deck:
			if i == 7:
				pre_deck.erase(i)
	if input == "Red Oni":
		for i in pre_deck:
			if i == 8:
				pre_deck.erase(i)
	if input == "Blue Oni":
		for i in pre_deck:
			if i == 9:
				pre_deck.erase(i)
	if input == "Priestess":
		for i in pre_deck:
			if i == 10:
				pre_deck.erase(i)
	if input == "Imp":
		for i in pre_deck:
			if i == 11:
				pre_deck.erase(i)
	if input == "False Angel":
		for i in pre_deck:
			if i == 12:
				pre_deck.erase(i)
	if input == "Queen Slime":
		for i in pre_deck:
			if i == 13:
				pre_deck.erase(i)
	if input == "Automaton":
		for i in pre_deck:
			if i == 14:
				pre_deck.erase(i)
	if input == "Sylph":
		for i in pre_deck:
			if i == 15:
				pre_deck.erase(i)
	if input == "Queen":
		for i in pre_deck:
			if i == 16:
				pre_deck.erase(i)

#func add_sc
#func remove_sc

func assign_chosen(player_deck):
	if player_deck == "p1":
		being_chosen = "p1"
	if player_deck == "p2":
		being_chosen = "p2"

func _on_WindowDialog_popup_hide():
	for i in get_children():
		if "Button" in i.name and i.pressed == true:
			i.pressed = false
	pre_deck = []
	pre_spell = null
	get_node("Text Input").clear()
	get_node("error").text = ""

func _on_WindowDialog_about_to_show():
	pre_deck = []
	pre_spell = null

func _on_Confirm_Button_pressed():
	confirm_check()

func confirm_check():
	var queen_present = false
	if typeof(pre_deck) != TYPE_ARRAY:
		get_node("error").text = "Deck cannot be understood."
		return
	for i in pre_deck:
		var same = 0
		if i == 16:
			queen_present = true
		if i > 16 or i < 0:
			get_node("error").text = "Deck cannot be understood."
			return
		for y in pre_deck:
			if y == i:
				same += 1
		if same > 1:
			get_node("error").text = "There is a duplicate card."
			return
		else:
			same = 0
	if pre_deck.size() == 6 and queen_present == true and (pre_spell != null and pre_spell >= 1 and pre_spell <= 6):
		if being_chosen == "p1":
			p1_deck = pre_deck
			emit_signal("p1_ready")
			emit_signal("send_p1", p1_deck)
			emit_signal("send_p1_spell", pre_spell)
			Notation.deck_print(p1_deck, "p1")
		if being_chosen == "p2":
			p2_deck = pre_deck
			emit_signal("p2_ready")
			emit_signal("send_p2", p2_deck)
			emit_signal("send_p2_spell", pre_spell)
			Notation.deck_print(p2_deck, "p2")
		confirm_close()
	elif pre_deck.size() > 6:
		get_node("error").text = "Deck is too large."
	elif pre_deck.size() < 6:
		get_node("error").text = "Deck is too small."
	elif queen_present != true:
		get_node("error").text = "Queen is missing from deck."
	elif pre_spell == null:
		get_node("error").text = "No Spell Chosen"
	elif pre_spell < 1 or pre_spell > 6:
		get_node("error").text = "Invalid Spell"
	else:
		get_node("error").text = "Deck cannot be understood."

func confirm_close():
	get_node("error").text = ""
	self.visible = false

func _on_Apprentice_Button_pressed():
	if get_node("Apprentice Button").pressed == true:
		add_to_deck("Apprentice")
	else:
		remove_from_deck("Apprentice")
func _on_Iron_Maiden_Button_pressed():
	if get_node("Iron Maiden Button").pressed == true:
		add_to_deck("Iron Maiden")
	else:
		remove_from_deck("Iron Maiden")
func _on_IttanMomen_Button_pressed():
	if get_node("IttanMomen Button").pressed == true:
		add_to_deck("IttanMomen")
	else:
		remove_from_deck("IttanMomen")
func _on_Nekomata_Button_pressed():
	if get_node("Nekomata Button").pressed == true:
		add_to_deck("Nekomata")
	else:
		remove_from_deck("Nekomata")
func _on_Harpy_Button_pressed():
	if get_node("Harpy Button").pressed == true:
		add_to_deck("Harpy")
	else:
		remove_from_deck("Harpy")
func _on_Redcap_Button_pressed():
	if get_node("Redcap Button").pressed == true:
		add_to_deck("Redcap")
	else:
		remove_from_deck("Redcap")
func _on_Slime_Button_pressed():
	if get_node("Slime Button").pressed == true:
		add_to_deck("Slime")
	else:
		remove_from_deck("Slime")
func _on_Holstaur_Button_pressed():
	if get_node("Holstaur Button").pressed == true:
		add_to_deck("Holstaur")
	else:
		remove_from_deck("Holstaur")
func _on_Red_Oni_Button_pressed():
	if get_node("Red Oni Button").pressed == true:
		add_to_deck("Red Oni")
	else:
		remove_from_deck("Red Oni")
func _on_Blue_Oni_Button_pressed():
	if get_node("Blue Oni Button").pressed == true:
		add_to_deck("Blue Oni")
	else:
		remove_from_deck("Blue Oni")
func _on_Priestess_Button_pressed():
	if get_node("Priestess Button").pressed == true:
		add_to_deck("Priestess")
	else:
		remove_from_deck("Priestess")
func _on_Imp_Button_pressed():
	if get_node("Imp Button").pressed == true:
		add_to_deck("Imp")
	else:
		remove_from_deck("Imp")
func _on_False_Angel_Button_pressed():
	if get_node("False Angel Button").pressed == true:
		add_to_deck("False Angel")
	else:
		remove_from_deck("False Angel")
func _on_Queen_Slime_Button_pressed():
	if get_node("Queen Slime Button").pressed == true:
		add_to_deck("Queen Slime")
	else:
		remove_from_deck("Queen Slime")
func _on_Automaton_Button_pressed():
	if get_node("Automaton Button").pressed == true:
		add_to_deck("Automaton")
	else:
		remove_from_deck("Automaton")
func _on_Sylph_Button_pressed():
	if get_node("Sylph Button").pressed == true:
		add_to_deck("Sylph")
	else:
		remove_from_deck("Sylph")
func _on_Queen_Button_pressed():
	if get_node("Queen Button").pressed == true:
		add_to_deck("Queen")
	else:
		remove_from_deck("Queen")

func _on_RB_Button_pressed():
	if get_node("RB Button").pressed == true:
		for i in get_children():
			if i.get_index() in [45,46,47,48,49]:
				i.pressed = false
		add_spell(1)
	else:
		add_spell(null)
func _on_P_Button_pressed():
	if get_node("P Button").pressed == true:
		for i in get_children():
			if i.get_index() in [44,46,47,48,49]:
				i.pressed = false
		add_spell(2)
	else:
		add_spell(null)
func _on_R_Button_pressed():
	if get_node("R Button").pressed == true:
		for i in get_children():
			if i.get_index() in [44,45,47,48,49]:
				i.pressed = false
		add_spell(3)
	else:
		add_spell(null)
func _on_SS_Button_pressed():
	if get_node("SS Button").pressed == true:
		for i in get_children():
			if i.get_index() in [44,45,46,48,49]:
				i.pressed = false
		add_spell(4)
	else:
		add_spell(null)
func _on_RE_Button_pressed():
	if get_node("RE Button").pressed == true:
		for i in get_children():
			if i.get_index() in [44,45,46,47,49]:
				i.pressed = false
		add_spell(5)
	else:
		add_spell(null)
func _on_FB_Button_pressed():
	if get_node("FB Button").pressed == true:
		for i in get_children():
			if i.get_index() in [44,45,46,47,48]:
				i.pressed = false
		add_spell(6)
	else:
		add_spell(null)



func _on_Apprentice_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Apprentice Sprite").visible = true
func _on_Apprentice_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Apprentice Sprite").visible = false
func _on_Iron_Maiden_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Iron Maiden Sprite").visible = true
func _on_Iron_Maiden_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Iron Maiden Sprite").visible = false
func _on_IttanMomen_Button_mouse_entered():
	get_parent().get_node("WindowDialog/IttanMomen Sprite").visible = true
func _on_IttanMomen_Button_mouse_exited():
	get_parent().get_node("WindowDialog/IttanMomen Sprite").visible = false
func _on_Nekomata_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Nekomata Sprite").visible = true
func _on_Nekomata_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Nekomata Sprite").visible = false
func _on_Harpy_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Harpy Sprite").visible = true
func _on_Harpy_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Harpy Sprite").visible = false
func _on_Redcap_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Redcap Sprite").visible = true
func _on_Redcap_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Redcap Sprite").visible = false
func _on_Slime_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Slime Sprite").visible = true
func _on_Slime_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Slime Sprite").visible = false
func _on_Holstaur_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Holstaur Sprite").visible = true
func _on_Holstaur_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Holstaur Sprite").visible = false
func _on_Red_Oni_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Red Oni Sprite").visible = true
func _on_Red_Oni_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Red Oni Sprite").visible = false
func _on_Blue_Oni_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Blue Oni Sprite").visible = true
func _on_Blue_Oni_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Blue Oni Sprite").visible = false
func _on_Priestess_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Priestess Sprite").visible = true
func _on_Priestess_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Priestess Sprite").visible = false
func _on_Imp_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Imp Sprite").visible = true
func _on_Imp_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Imp Sprite").visible = false
func _on_False_Angel_Button_mouse_entered():
	get_parent().get_node("WindowDialog/False Angel Sprite").visible = true
func _on_False_Angel_Button_mouse_exited():
	get_parent().get_node("WindowDialog/False Angel Sprite").visible = false
func _on_Queen_Slime_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Queen Slime Sprite").visible = true
func _on_Queen_Slime_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Queen Slime Sprite").visible = false
func _on_Automaton_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Automaton Sprite").visible = true
func _on_Automaton_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Automaton Sprite").visible = false
func _on_Sylph_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Sylph Sprite").visible = true
func _on_Sylph_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Sylph Sprite").visible = false
func _on_Queen_Button_mouse_entered():
	get_parent().get_node("WindowDialog/Queen Sprite").visible = true
func _on_Queen_Button_mouse_exited():
	get_parent().get_node("WindowDialog/Queen Sprite").visible = false
func _on_RB_Button_mouse_entered():
	get_parent().get_node("WindowDialog/RB Sprite").visible = true
func _on_RB_Button_mouse_exited():
	get_parent().get_node("WindowDialog/RB Sprite").visible = false
func _on_P_Button_mouse_entered():
	get_parent().get_node("WindowDialog/P Sprite").visible = true
func _on_P_Button_mouse_exited():
	get_parent().get_node("WindowDialog/P Sprite").visible = false
func _on_R_Button_mouse_entered():
	get_parent().get_node("WindowDialog/R Sprite").visible = true
func _on_R_Button_mouse_exited():
	get_parent().get_node("WindowDialog/R Sprite").visible = false
func _on_SS_Button_mouse_entered():
	get_parent().get_node("WindowDialog/SS Sprite").visible = true
func _on_SS_Button_mouse_exited():
	get_parent().get_node("WindowDialog/SS Sprite").visible = false
func _on_RE_Button_mouse_entered():
	get_parent().get_node("WindowDialog/RE Sprite").visible = true
func _on_RE_Button_mouse_exited():
	get_parent().get_node("WindowDialog/RE Sprite").visible = false
func _on_FB_Button_mouse_entered():
	get_parent().get_node("WindowDialog/FB Sprite").visible = true
func _on_FB_Button_mouse_exited():
	get_parent().get_node("WindowDialog/FB Sprite").visible = false

