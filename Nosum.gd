extends Node

#Will contain cells and responsible pieces in a vector, all of which will be in something that can dynamically grow and shrink
#one in gives piece, three in [1] gives piece id, four in gives individual nsc, two is [0] placeholder and 3 is too for nsc
var p1_nsz = []
var p2_nsz = []

signal nsz_send(nszs)

#calculates ns cells(nsc) 
#called by summoning(current_cell passed) and after movement(final_cell passed)/remove_nsz
func nsz_calc(active_player, new_cell, piece):
	var nscp = []
	var nscs = []
	var nsc
	if piece.nsre != null:
		for i in piece.nsre:
			nsc = new_cell.coordinate + i
			nscs.append(nsc)
	if piece.nsro != null:
		for i in piece.nsro:
			if int(new_cell.coordinate.y) % 2 == 0: #even initial row
				nsc = new_cell.coordinate + Vector2(i[0][0], i[1])
			elif int(new_cell.coordinate.y) % 2:
				nsc = new_cell.coordinate + Vector2(i[0][1], i[1])
			nscs.append(nsc)
	nscp.append([nscs, piece.id])
	add_nsz(nscp, piece.id)
			


func add_nsz(nscp, piece_id): #adds what nsz_calc outputs to appropriate variable
	if "p1" in piece_id:
		p1_nsz.append(nscp)
	else:
		p2_nsz.append(nscp)

func remove_nsz(removed_piece): #called after capturing, movement and sacrifice, receives id
	if "p1" in removed_piece.id:
		for i in p1_nsz:
			if removed_piece.id == i[0][1]:
				p1_nsz.erase(i)
	else:
		for i in p2_nsz:
			if removed_piece.id == i[0][1]:
				p2_nsz.erase(i)
			

#helps determine if summoning is possible, will return a true of false value
func nsz_check(active_player, current_cell): #called by menu and eventually notation 
	var this_nsz_check 
	if active_player.name == "p1":
		for x in p2_nsz:
			for y in x:
				for z in y[0]:
					if current_cell.coordinate == z:
							this_nsz_check = true
	if active_player.name == "p2":
		for x in p1_nsz:
			for y in x:
				for z in y[0]:
					if current_cell.coordinate == z:
						this_nsz_check = true
	return this_nsz_check

#sends nsz to board
func nsz_package(chosen_player):
	if chosen_player == "p1":
		emit_signal("nsz_send", p1_nsz)
	else:
		emit_signal("nsz_send", p2_nsz)

func reset():
	p1_nsz = []
	p2_nsz = []
