extends Node

var active_player
 
var p1_container = []
var p2_container = []

var p1_site1 #cells where pieces where sacrificed
var p1_site2

var p2_site1
var p2_site2

var turn_count = 0 
var p1_sacrifice_turn = 0
var p2_sacrifice_turn = 0

var p1_material = ["empty","empty"] #piece types of sacrifices
var p2_material = ["empty","empty"]

var p1_reg_effect = 0
var p2_reg_effect = 0

signal reg_validate

var dre = [Vector2(1,0), Vector2(2,0), Vector2(-1,0), Vector2(-2,0), Vector2(1,2), Vector2(2,2), Vector2(-1,2), 
	Vector2(-2,2), Vector2(1,-2), Vector2(2,-2), Vector2(-1,-2), Vector2(-2,-2)]
var dro = [ [[0,-1], 1], [[1,0], 1], [[-1,-2], 1], [[2,1], 1], [[0,-1], -1], [[1,0], -1], [[-1,-2], -1], [[2,1], -1] ]

#add another 2 variables for regicide which is just adjacent cells, same except no arrays with 2 in it
var fel_dre =  [Vector2(1,0),  Vector2(-1,0)]
var fel_dro = [ [[0,-1], 1], [[1,0], 1], [[0,-1], -1], [[1,0], -1]]


func get_sacrifice(cell):
	var occupying_piece
	if "p1" in cell.occupying:
		for i in p1_container.get_children():
			if i.id == cell.occupying:
				occupying_piece = i

						
	if "p2" in cell.occupying:
		for i in p2_container.get_children():
			if i.id == cell.occupying:
				occupying_piece = i
	sacrifice(occupying_piece, cell)

#the menu will control what can/can't be sacrificed, it might call here and check a function for a returned true value
func sacrifice(input_piece, site): #site is cell sacrifice was performed
	
	if input_piece == null:
		return
		
	if "p1" in input_piece.name:
		if p1_material[0] == "empty":
			p1_material[0] = input_piece.piece_type
			p1_site1 = site
			remove_piece(input_piece, site, "p1")
		else:
			p1_material[1] = input_piece.piece_type
			p1_site2 = site
			remove_piece(input_piece, site, "p1")
			p1_sacrifice_turn = turn_count
	if "p2" in input_piece.name:
		if p2_material[0] == "empty":
			p2_material[0] = input_piece.piece_type
			p2_site1 = site
			remove_piece(input_piece, site, "p2")
		else:
			p2_material[1] = input_piece.piece_type
			p2_site2 = site
			remove_piece(input_piece, site, "p2")
			p2_sacrifice_turn = turn_count
	
	Notation.sac_print(input_piece.piece_type, site.name)

	if (((active_player == "p1" and p1_reg_effect == 1) or (active_player == "p2" and p2_reg_effect == 1)) and
		input_piece.piece_type == "Queen"):
		emit_signal("reg_validate", active_player)
	

func remove_piece(input_piece, site, owner_player):
	Nosum.remove_nsz(input_piece)
	Release.release(input_piece, owner_player)
	input_piece.queue_free()
	site.occupying = null

	
func sacrifice_reset():
	#will probably need to readjust
	if p1_sacrifice_turn != 0 and turn_count > p1_sacrifice_turn + 2:
		p1_site1 = null
		p1_site1 = null
		p1_material = ["empty","empty"]
		p1_sacrifice_turn = 0
	if p2_sacrifice_turn != 0 and turn_count > p2_sacrifice_turn + 2:
		p2_site1 = null
		p2_site1 = null
		p2_material = ["empty","empty"]
		p2_sacrifice_turn = 0

func update_p1_container(new_container):
	p1_container = new_container
func update_p2_container(new_container):
	p2_container = new_container

#func sacrifice check, evaluates distance of current pieces in player's container or past sacrifice 
func distance_check(piece_id, piece_coordinate):
	var inter_check
	if "p1" in piece_id:
		if p1_site1 == null:
			for i in p1_container.get_children():
			#checking to see if there is any piece close enough
				if i.id != piece_id: 
					inter_check = e_dist_check(piece_coordinate, i.cur_coor, dre)
				if inter_check != true:
					inter_check = o_dist_check(piece_coordinate, i.cur_coor, dro)
				if inter_check == true:
					break
				else:
					inter_check = false
		#if there is not, checking to see if a previously sacrificed peice is close enough
		else:
			inter_check = e_dist_check(piece_coordinate, p1_site1.coordinate, dre)
			if inter_check != true:
				inter_check = o_dist_check(piece_coordinate, p1_site1.coordinate, dro)

		
	if "p2" in piece_id:
		if p2_site1 == null:
			for i in p2_container.get_children():
			#checking to see if there is any piece close enough
				if i.id != piece_id: 
					inter_check = e_dist_check(piece_coordinate, i.cur_coor, dre)
				if inter_check != true:
					inter_check = o_dist_check(piece_coordinate, i.cur_coor, dro)
				if inter_check == true:
					break
				else:
					inter_check = false
		#if there is not, checking to see if a previously sacrificed peice is close enough
		else:
			inter_check = e_dist_check(piece_coordinate, p2_site1.coordinate, dre)
			if inter_check != true:
				inter_check = o_dist_check(piece_coordinate, p2_site1.coordinate, dro)

	return inter_check

func n_distance_check(first_coor, second_coor):
	var inter_check
	inter_check = e_dist_check(first_coor, second_coor, dre)
	if inter_check != true:
		inter_check = o_dist_check(first_coor, second_coor, dro)
	return inter_check

func material_check(piece_type, active_player, cell):
	var inter_check
	var inter_material
	var inter_site1
	var inter_site2
	if active_player.name == "p1":
		inter_material = p1_material
		inter_site1 = p1_site1
		inter_site2 = p1_site2
	if active_player.name == "p2":
		inter_material = p2_material
		inter_site1 = p2_site1
		inter_site2 = p2_site2
	
	if inter_site1 == null or inter_site2 == null:
		return
	
	if piece_type == "Queen Slime":
		if inter_material[0] == "Slime" or inter_material[1] == "Slime":
			if cell.coordinate == inter_site1.coordinate or cell.coordinate == inter_site2.coordinate:
				inter_check = true
	if piece_type == "Automaton":
		if inter_material[0] == "Iron Maiden" or inter_material[1] == "Iron Maiden":
			if cell.coordinate == inter_site1.coordinate or cell.coordinate == inter_site2.coordinate:
				inter_check = true
	if piece_type == "Sylph":
		if inter_material[0] == "Harpy" or inter_material[1] == "Harpy":
			if cell.coordinate == inter_site1.coordinate or cell.coordinate == inter_site2.coordinate:
				inter_check = true

	return inter_check

func set_turn_count():
	turn_count += 1
	sacrifice_reset()
	if turn_count % 2:
		#these are strings
		active_player = "p1"
	else:
		active_player = "p2"

func e_dist_check(pc, tc, rel): #piece coordinate, compared coordinate 
	var dist_check = false
	var try
	
	for x in rel:
		try = pc + x
		if try == tc:
			dist_check = true
			break
			
	if dist_check != true:
		dist_check = false
	return dist_check

func o_dist_check(pc, tc, rel):
	var dist_check = false
	var try
	
	for x in rel:
		if int(pc.y) % 2 == 0: #even initial row
			try  = pc + Vector2(x[0][0], x[1])
		elif int(pc.y) % 2: 
			try = pc + Vector2(x[0][1], x[1])
			
		if try == tc:
			dist_check = true
			break
			
	return dist_check

func reset():
	active_player = null
 
	p1_container = []
	p2_container = []

	p1_site1 = null
	p1_site2 = null

	p2_site1 = null
	p2_site2 = null

	turn_count = 0 
	p1_sacrifice_turn = 0
	p2_sacrifice_turn = 0

	p1_material = ["empty","empty"] #piece types of sacrifices
	p2_material = ["empty","empty"]

	p1_reg_effect = 0
	p2_reg_effect = 0
