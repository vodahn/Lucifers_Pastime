extends Node

#reduces ammount of piece type on board for number limit 
func release(piece, owner_player):
	var pt = piece.piece_type
	if pt != "Queen":
		if owner_player == "p1":
			if pt == "Apprentice":
				Apprentice.p1_amm -= 1
			if pt == "Iron Maiden":
				IronMaiden.p1_amm -= 1
			if pt == "IttanMomen":
				IttanMomen.p1_amm -= 1
			if pt == "Nekomata":
				Nekomata.p1_amm -= 1
			if pt == "Harpy":
				Harpy.p1_amm -= 1
			if pt == "Redcap":
				Redcap.p1_amm -= 1
			if pt == "Slime":
				Slime.p1_amm -= 1
			if pt == "Holstaur":
				Holstaur.p1_amm -= 1
			if pt == "Red Oni":
				RedOni.p1_amm -= 1
			if pt == "Blue Oni":
				BlueOni.p1_amm -= 1
			if pt == "Priestess":
				Priestess.p1_amm -= 1
			if pt == "Imp":
				Imp.p1_amm -= 1
			if pt == "False Angel":
				FalseAngel.p1_amm -= 1
			if pt == "Queen Slime":
				QueenSlime.p1_amm -= 1
			if pt == "Automataon":
				Automaton.p1_amm -= 1
			if pt == "Sylph":
				Sylph.p1_amm -= 1
		else:
			if pt == "Apprentice":
				Apprentice.p2_amm -= 1
			if pt == "Iron Maiden":
				IronMaiden.p2_amm -= 1
			if pt == "IttanMomen":
				IttanMomen.p2_amm -= 1
			if pt == "Nekomata":
				Nekomata.p2_amm -= 1
			if pt == "Harpy":
				Harpy.p2_amm -= 1
			if pt == "Redcap":
				Redcap.p2_amm -= 1
			if pt == "Slime":
				Slime.p2_amm -= 1
			if pt == "Holstaur":
				Holstaur.p2_amm -= 1
			if pt == "Red Oni":
				RedOni.p2_amm -= 1
			if pt == "Blue Oni":
				BlueOni.p2_amm -= 1
			if pt == "Priestess":
				Priestess.p2_amm -= 1
			if pt == "Imp":
				Imp.p2_amm -= 1
			if pt == "False Angel":
				FalseAngel.p2_amm -= 1
			if pt == "Queen Slime":
				QueenSlime.p2_amm -= 1
			if pt == "Automataon":
				Automaton.p2_amm -= 1
			if pt == "Sylph":
				Sylph.p2_amm -= 1
