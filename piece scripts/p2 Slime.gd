extends "res://Piece.gd"

var nsre = null 
var nsro = null

var lre = null
var lro = null
var path_relations = [Vector2(2,2), Vector2(2,3), Vector2(2,5), Vector2(2,6)]
var cur_coor

func _init():
	piece_type = "Slime"
	cost = 10

var dragging = false

signal dragsignal;

func _ready():
	connect("dragsignal",self,"_set_drag_pc")
	
	
func _process(delta):
	if dragging:
		var mousepos = get_viewport().get_mouse_position()
		self.position = Vector2(mousepos.x, mousepos.y)

		

func _set_drag_pc():
	dragging=!dragging


func _on_p2_Slime_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and dragging == false:
			emit_signal("dragsignal")
			emit_signal("move_start", self)
		elif event.button_index == BUTTON_LEFT and !event.pressed and dragging == true:
			emit_signal("dragsignal")
			emit_signal("move_end")
			
signal move_start(input_piece)
signal move_end

func incr_amm():
	Slime.p2_amm += 1
func decr_amm():
	Slime.p2_amm -= 1
