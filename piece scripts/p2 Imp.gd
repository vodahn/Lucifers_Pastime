extends "res://Piece.gd"

var nsre = [ Vector2(0,2), Vector2(0, -2) ]
var nsro = [  [[2,1], 1], [[-1,-2], 1], [[2,1], -1], [[-1,-2], -1]]

var lre = [Vector2(0,-2)]
var lro = [ [[2,1], 1], [[-1,-2], 1], [[2,1], -1], [[-1,-2], -1] ]
var path_relations = [Vector2(1,2)]
var cur_coor

func _init():
	piece_type = "Imp"
	cost = 20

var dragging = false

signal dragsignal;

func _ready():
	connect("dragsignal",self,"_set_drag_pc")
	
	
func _process(delta):
	if dragging:
		var mousepos = get_viewport().get_mouse_position()
		self.position = Vector2(mousepos.x, mousepos.y)

		

func _set_drag_pc():
	dragging=!dragging


func _on_p2_Imp_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and dragging == false:
			emit_signal("dragsignal")
			emit_signal("move_start", self)
		elif event.button_index == BUTTON_LEFT and !event.pressed and dragging == true:
			emit_signal("dragsignal")
			emit_signal("move_end")
			
signal move_start(input_piece)
signal move_end

func incr_amm():
	Imp.p2_amm += 1
func decr_amm():
	Imp.p2_amm -= 1
