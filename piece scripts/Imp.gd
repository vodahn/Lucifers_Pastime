extends "res://Piece.gd"

var p1_piece = preload("res://piece scenes/p1 Imp.tscn")
var p2_piece = preload("res://piece scenes/p2 Imp.tscn")

var p1_number = ["p1", 0]
var p2_number = ["p2", 0]

func _init():
	piece_type = "Imp"
	cost = 20

var p1_amm  = 0
var p2_amm = 0
