extends "res://Piece.gd"

var nsre = [ Vector2(-1,0), Vector2(1,0) ] 
var nsro = null

var lre = [Vector2(0,-2)]
var lro = null
var path_relations = [Vector2(1,2), Vector2(1,3), Vector2(1,1), Vector2(1,4)]
var cur_coor

func _init():
	piece_type = "Red Oni"
	cost = 10

var dragging = false

signal dragsignal;

func _ready():
	connect("dragsignal",self,"_set_drag_pc")
	
	
func _process(delta):
	if dragging:
		var mousepos = get_viewport().get_mouse_position()
		self.position = Vector2(mousepos.x, mousepos.y)

		

func _set_drag_pc():
	dragging=!dragging


func _on_p1_Red_Oni_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and dragging == false:
			emit_signal("dragsignal")
			emit_signal("move_start", self)
		elif event.button_index == BUTTON_LEFT and !event.pressed and dragging == true:
			emit_signal("dragsignal")
			emit_signal("move_end")
			
signal move_start(input_piece)
signal move_end

func incr_amm():
	RedOni.p1_amm += 1
func decr_amm():
	RedOni.p1_amm -= 1
