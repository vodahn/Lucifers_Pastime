extends "res://Piece.gd"

#like leap relations, there is different rules for even and odd y spacing 
var nsre = null 
var nsro = [ [[1,0], 1], [[0,-1], 1] ]

var lre = null
var lro = null
var path_relations = [Vector2(1,2), Vector2(1,3)]
var cur_coor

func _init():
	piece_type = "Apprentice"
	cost = 2

var dragging = false

signal dragsignal;

func _ready():
	connect("dragsignal",self,"_set_drag_pc")
	
	
func _process(_delta):
	if dragging:
		var mousepos = get_viewport().get_mouse_position()
		self.position = Vector2(mousepos.x, mousepos.y)

		

func _set_drag_pc():
	dragging=!dragging


func _on_p1_Apprentice_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed and dragging == false:
			emit_signal("dragsignal")
			emit_signal("move_start", self)
		elif event.button_index == BUTTON_LEFT and !event.pressed and dragging == true:
			emit_signal("dragsignal")
			emit_signal("move_end")
			
signal move_start(input_piece)
signal move_end

func incr_amm():
	Apprentice.p1_amm += 1
func decr_amm():
	Apprentice.p1_amm -= 1
