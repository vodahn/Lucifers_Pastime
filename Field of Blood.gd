extends Node

func effect(active_player):
	if active_player == "p1":
		Movement.p1_fel_effect = 1
		Summoning.p1_fel_effect = 1
		Notation.p1_fel_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_effect = 1
	else:
		Movement.p2_fel_effect = 1
		Summoning.p2_fel_effect = 1
		Notation.p2_fel_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p2_fel_effect = 1

	get_parent().end_turn()
	yield()

func effect2(active_player, capd, capr):
	print("fel 2")
	if active_player == "p1":
		Movement.p1_fel_effect = 2
		Summoning.p1_fel_effect = 2
		Notation.p1_fel_effect = 2
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_effect = 2
		
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_capd = capd
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_capr = capr
		Summoning.p1_fel_capd = capd
		Summoning.p1_fel_capr = capr
		Notation.p1_fel_capd = capd
		Notation.p1_fel_capr = capr
	else:
		Movement.p2_fel_effect = 2
		Summoning.p2_fel_effect = 2
		Notation.p2_fel_effect = 2
		get_parent().get_parent().get_node("Context Menu/Menu").p2_fel_effect = 2
		
		get_parent().get_parent().get_node("Context Menu/Menu").p2_fel_capd = capd
		get_parent().get_parent().get_node("Context Menu/Menu").p2_fel_capr = capr
		Summoning.p2_fel_capd = capd
		Summoning.p2_fel_capr = capr
		Notation.p2_fel_capd = capd
		Notation.p2_fel_capr = capr

func effect_reset(active_player):
	if active_player == "p1":
		Movement.p1_fel_effect = 0
		Summoning.p1_fel_effect = 0
		Notation.p1_fel_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_effect = 0
		
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_capd = null
		get_parent().get_parent().get_node("Context Menu/Menu").p1_fel_capr = null
		Summoning.p1_fel_capd = null
		Summoning.p1_fel_capr = null
		Notation.p1_fel_capd = null
		Notation.p1_fel_capr = null
	else:
		Movement.p2_fel_effect = 0
		Summoning.p2_fel_effect = 0
		Notation.p2_fel_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p2_fel_effect = 0
