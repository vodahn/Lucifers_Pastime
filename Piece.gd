extends Node2D

var A = "A"
var I = "I"
var N = "N"
var It = "It"
var S = "S"
var Rc = "Rc"
var Hs = "Hs"
var H = "H"
var Ro = "Ro"
var B = "B"
var P = "P"
var Im = "Im"
var F = "F"
var Qs = "Qs"
var Au = "Au"
var Sy = "Sy"
var Q = "Q"
var D = "D"

var pdic = {A: "Apprentice", I: "Iron Maiden", N: "Nekomata", It: "IttanMomen", 
S: "Slime", Rc: "Redcap", Hs: "Holstaur", H: "Harpy", Ro: "Red Oni", B: "Blue Oni",
P: "Priestess", Im: "Imp", F: "False Angel", Qs: "Queen Slime", Au: "Automaton",
Sy: "Sylph", Q: "Queen", D: "default"}

var piece_type = "default"
var id

#var player_number, a global variable with p1 or p2 and player_number, every piece will have its own

var pt #transition from name of piece, to piece type in id 

var cost
var movement_pattern
var no_summong_pattern

func id_setup(player_number):
	#
	for key in pdic:
		if piece_type == pdic[key]:
			pt = key
			break

	if player_number[0] == "p1":
		id_maker(pt, player_number[0], player_number[1])
	if player_number[0] == "p2":
		id_maker(pt, player_number[0], player_number[1])
	else:
		id_maker(pt, player_number[0], player_number[1])

func id_maker(type, player, piece_player_number):
	id = player + type + str(piece_player_number)
	
func instance() -> Node:
	# Nodes have a `filename` property. Scene root nodes have this
	# set to the path of the scene file from which they were instantiated.
	var scn := load(filename) as PackedScene
	if not scn:
		push_error("There were problems loading the scene again.")
	return scn.instance()

