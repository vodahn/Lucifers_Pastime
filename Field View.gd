extends Node

#will allow player to view their and opponent's no summoning zones 
#when the button 1 or 2 is pressed, approprate field shown, and updated every turn depending on showing var
var showing = null


signal nsz_ask(chosen_player)
signal field_hide #sent to board which makes all cells play default animation, will get no summoning zones from Nosum

func _ready(): 
	set_process_input(true)
	
func _input(event):
	if event.is_action_pressed("ui_one") and get_parent().get_node("CanvasLayer/Deck Edit/WindowDialog").visible == false:
		if showing == null:
			emit_signal("nsz_ask", "p1")
			showing = "p1"
		elif showing == "p2": 
			emit_signal("field_hide")
			emit_signal("nsz_ask", "p1")
			showing = "p1"
		else:
			emit_signal("field_hide")
			showing = null
	if event.is_action_pressed("ui_two") and get_parent().get_node("CanvasLayer/Deck Edit/WindowDialog").visible == false:
		if showing == null:
			emit_signal("nsz_ask", "p2")
			showing = "p2"
		elif showing == "p1": 
			emit_signal("field_hide")
			emit_signal("nsz_ask", "p2")
			showing = "p2"
		else:
			emit_signal("field_hide")
			showing = null
	if event.is_action_pressed("ui_four"):
		if get_parent().get_node("Mouse Script").show_cell == false:
			get_parent().get_node("Mouse Script").show_cell = true
		else:
			get_parent().get_node("Mouse Script").show_cell = false
			get_parent().get_node("cell label").text = ""

#hides field before updating it. 
func update_field(): #callled every turn
	if showing == "p1":
		emit_signal("field_hide")
		emit_signal("nsz_ask", "p1")
	if showing == "p2":
		emit_signal("field_hide")
		emit_signal("nsz_ask", "p2")

func reset():
	showing = null
