extends Node

#resets variables in every script with a turn count, except decks which are retained 
signal reset

signal setup_done
signal board_enable

func reset():
	emit_signal("reset")
	yield(get_tree().create_timer(.3), "timeout")
	emit_signal("setup_done")
	emit_signal("board_enable")
