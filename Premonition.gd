extends Node


func effect(active_player):
	#active_player received here should be a string
	if active_player == "p1":
		Movement.p1_prem_effect = 1
		Movement.mov_block = true
		Summoning.p1_prem_effect = 1
		Notation.p1_prem_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p1_prem_effect = 1
	else:
		Movement.p2_prem_effect = 1
		Movement.mov_block = true
		Summoning.p2_prem_effect = 1
		Notation.p2_prem_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p2_prem_effect = 1
	get_parent().end_turn()
	yield()
	

func effect2(active_player):
	if active_player == "p1":
		Movement.p1_prem_effect = 2
		Summoning.p1_prem_effect = 2
		Notation.p1_prem_effect = 2
		Movement.mov_block = false
		get_parent().get_parent().get_node("Context Menu/Menu").p1_prem_effect = 2
	else:
		Movement.p2_prem_effect = 2
		Summoning.p2_prem_effect = 2
		Notation.p2_prem_effect = 2
		Movement.mov_block = false
		get_parent().get_parent().get_node("Context Menu/Menu").p2_prem_effect = 2

func effect_reset(active_player):
	if active_player == "p1":
		Movement.p1_prem_effect = 0
		Summoning.p1_prem_effect = 0
		Notation.p1_prem_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p1_prem_effect = 0
	else:
		Movement.p2_prem_effect = 0
		Summoning.p2_prem_effect = 0
		Notation.p2_prem_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p2_prem_effect = 0
	Summoning.summ_block = false
