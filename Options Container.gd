extends HBoxContainer

var deck_container_lock = null
var game_container_lock = null

var p1_ready = false
var p2_ready = false

var turn_count = 0

signal being_chosen(player)
signal open_editor
signal forfeit(winner)

func _ready():
	get_node("Deck Container/p1 Deck").visible = false
	get_node("Deck Container/p1 Deck").mouse_filter = 2
	get_node("Deck Container/p2 Deck").visible = false
	get_node("Deck Container/p2 Deck").mouse_filter = 2
	get_node("Game Container/New").visible = false
	get_node("Game Container/New").mouse_filter = 2
	set_process(true)

func _process(_delta):
	if deck_container_lock == null:
		get_node("Deck Container/p1 Deck").visible = false
		get_node("Deck Container/p1 Deck").mouse_filter = 2
		get_node("Deck Container/p2 Deck").visible = false
		get_node("Deck Container/p2 Deck").mouse_filter = 2
		if game_container_lock == null:
			if get_parent().get_parent().get_node("Notation Container/Notation Box").mouse_filter == 2:
				get_parent().get_parent().get_node("Notation Container/Notation Box").mouse_filter = 0
		else:
			get_parent().get_parent().get_node("Notation Container/Notation Box").mouse_filter = 2
	if game_container_lock == null:
		get_node("Game Container/New").visible = false
		get_node("Game Container/New").mouse_filter = 2
		get_node("Game Container/Forfeit").visible = false
		get_node("Game Container/Forfeit").mouse_filter = 2
		if deck_container_lock == null:
			if get_parent().get_parent().get_node("Notation Container/Notation Box").mouse_filter == 2:
				get_parent().get_parent().get_node("Notation Container/Notation Box").mouse_filter = 0
		else:
			get_parent().get_parent().get_node("Notation Container/Notation Box").mouse_filter = 2



func _on_Deck_mouse_entered():
	deck_container_lock = "deck"
	if turn_count > 0:
		get_node("Deck Container/Deck").set_disabled(true)
		get_node("Deck Container/Deck").mouse_filter = 2
		return
	else:
		get_node("Deck Container/Deck").set_disabled(false)
		get_node("Deck Container/Deck").mouse_filter = 1

	get_node("Deck Container/p1 Deck").visible = true
	get_node("Deck Container/p1 Deck").mouse_filter = 1
	get_node("Deck Container/p2 Deck").visible = true
	get_node("Deck Container/p2 Deck").mouse_filter = 1

	if p1_ready == true:
		get_node("Deck Container/p1 Deck/check").visible = true
	if p2_ready == true:
		get_node("Deck Container/p2 Deck/check").visible = true

func _on_Deck_mouse_exited():
	yield(get_tree().create_timer(.05), "timeout")
	if deck_container_lock != "p1 deck" and deck_container_lock != "p2 deck":
		deck_container_lock = null

func _on_p1_Deck_mouse_entered():
	deck_container_lock = "p1 deck"
func _on_p1_Deck_mouse_exited():
	yield(get_tree().create_timer(.1), "timeout")
	if deck_container_lock != "p2 deck" and deck_container_lock != "deck":
		deck_container_lock = null

func _on_p2_Deck_mouse_entered():
	deck_container_lock = "p2 deck"
func _on_p2_Deck_mouse_exited():
	yield(get_tree().create_timer(.1), "timeout")
	if deck_container_lock != "p1 deck" and deck_container_lock != "deck":
		deck_container_lock = null


func _on_Game_mouse_entered():
	game_container_lock = "game"
	get_node("Game Container/New").visible = true
	get_node("Game Container/New").mouse_filter = 1
	get_node("Game Container/Forfeit").visible = true
	get_node("Game Container/Forfeit").mouse_filter = 1
	if p1_ready == false or p2_ready == false:
		get_node("Game Container/New").set_disabled(true)
	elif turn_count > 0:
		get_node("Game Container/New").set_disabled(true)
	else:
		get_node("Game Container/New").set_disabled(false)
	
	if turn_count == 0:
		get_node("Game Container/Forfeit").set_disabled(true)
	else:
		get_node("Game Container/Forfeit").set_disabled(false)
		
func _on_Game_mouse_exited():
	yield(get_tree().create_timer(.05), "timeout")
	if game_container_lock != "New" and game_container_lock != "forfeit":
		game_container_lock = null

func _on_New_mouse_entered():
	game_container_lock = "New"
func _on_New_mouse_exited():
	yield(get_tree().create_timer(.1), "timeout")
	if game_container_lock != "game" and game_container_lock != "forfeit":
		game_container_lock = null

func _on_Forfeit_mouse_entered():
	game_container_lock = "forfeit"
func _on_Forfeit_mouse_exited():
	yield(get_tree().create_timer(.1), "timeout")
	if game_container_lock != "game" and game_container_lock != "New":
		game_container_lock = null



func _on_p1_Deck_pressed():
	emit_signal("being_chosen", "p1")
	emit_signal("open_editor")

func _on_p2_Deck_pressed():
	emit_signal("being_chosen", "p2")
	emit_signal("open_editor")

func _on_New_pressed():
	get_parent().get_parent().get_node("CanvasLayer4/Orientation Dialog/WindowDialog4").popup_centered()
	get_node("Game Container/New").visible = false
	NewGame.reset()
	game_container_lock = null
	get_node("Game Container/New").mouse_filter = 2
	get_node("Deck Container/Deck").set_disabled(true)

func _on_Forfeit_pressed():
	var player
	if turn_count % 2 == 0:
		emit_signal("forfeit", "p1f")
	else:
		emit_signal("forfeit", "p2f")

	get_node("Game Container/Forfeit").visible = false
	game_container_lock = null
	get_node("Game Container/Forfeit").mouse_filter = 2


func p1_is_ready():
	p1_ready = true

func p2_is_ready():
	p2_ready = true

func set_turn_count():
	turn_count += 1


func reset():
	deck_container_lock = null
	game_container_lock = null
	
	turn_count = 0
