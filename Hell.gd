extends Node
#currently being used for testing components

func _ready(): 

	#menu showing
	get_node("Mouse Script").connect("menu_show", get_node("Context Menu/Menu"), "show_menu")
	get_node("Mouse Script").connect("menu_hide", get_node("Context Menu/Menu"), "hide_menu")
	
	#ending a turn 
	get_node("Context Menu/Menu").connect("turn_done", get_node("TurnQueue"), "change_player")
	get_node("Context Menu/Menu").connect("turn_done", get_node("TurnQueue"), "turn_count_increase")
	Movement.connect("turn_done", get_node("TurnQueue"), "change_player")
	Movement.connect("turn_done", get_node("TurnQueue"), "turn_count_increase")
	Notation.connect("turn_done", get_node("TurnQueue"), "change_player")
	Notation.connect("turn_done", get_node("TurnQueue"), "turn_count_increase")
	get_node("Spell Card").connect("turn_done", get_node("TurnQueue"), "change_player")
	get_node("Spell Card").connect("turn_done", get_node("TurnQueue"), "turn_count_increase")
	NewGame.connect("setup_done", get_node("TurnQueue"), "turn_count_increase")
	
	
	#updating turn everywhere 
	get_node("TurnQueue").connect("turn_update", get_node("Field View"), "update_field")
	get_node("TurnQueue").connect("turn_update", Sacrifice, "set_turn_count")
	get_node("TurnQueue").connect("turn_update", Movement, "set_turn_count")
	get_node("TurnQueue").connect("turn_update", Notation, "set_turn_count")
	get_node("TurnQueue").connect("turn_update", get_node("Board"), "set_turn_count")
	get_node("TurnQueue").connect("turn_update", get_node("Context Menu/Menu"), "set_turn_count")
	get_node("TurnQueue").connect("turn_update", get_node("Spell Card"), "set_turn_count")
	get_node("TurnQueue").connect("turn_update", get_node("Notation Container/Input"), "set_turn_count")
	get_node("TurnQueue").connect("turn_update", get_node("Options/Options Container"), "set_turn_count")
	
	#mouse info to movement
	Movement.connect("get_initial_cell", get_node("Mouse Script"), "send_initial_cell")
	Movement.connect("get_final_cell", get_node("Mouse Script"), "send_final_cell")
	get_node("Mouse Script").connect("send_icell", Movement, "assign_initial_cell")
	get_node("Mouse Script").connect("send_fcell", Movement, "assign_final_cell")
	
	#zone and mp visuals 
	get_node("Field View").connect("nsz_ask", Nosum, "nsz_package")
	Nosum.connect("nsz_send", get_node("Board"), "nsz_apply")
	get_node("Field View").connect("field_hide", get_node("Board"), "field_hide")

	Movement.connect("try_send", get_node("Board"), "mov_show")
	Movement.connect("mov_hide", get_node("Board"), "mov_hide")
	
	Summoning.connect("mp_display", get_node("TurnQueue"), "mp_display")
	
	#deck selection
	get_node("Options/Options Container").connect("being_chosen", get_node("CanvasLayer/Deck Edit/WindowDialog"), "assign_chosen")
	get_node("Options/Options Container").connect("open_editor", get_node("CanvasLayer/Deck Edit/WindowDialog"), "editor_open")
	get_node("Notation Container/Input").connect("being_chosen", get_node("CanvasLayer/Deck Edit/WindowDialog"), "assign_chosen")
	get_node("Notation Container/Input").connect("deck_input", get_node("CanvasLayer/Deck Edit/WindowDialog/Text Input"), "ninput")
	
	#receiving deck info
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("p1_ready", get_node("Options/Options Container"), "p1_is_ready")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("p2_ready", get_node("Options/Options Container"), "p2_is_ready")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p1", get_node("Context Menu/Menu"), "receive_p1")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p2", get_node("Context Menu/Menu"), "receive_p2")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p1", Notation, "receive_p1")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p2", Notation, "receive_p2")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p1", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p1")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p2", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p2")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p1_spell", get_node("Spell Card"), "receive_p1_spell")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p2_spell", get_node("Spell Card"), "receive_p2_spell")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p1_spell", Notation, "receive_p1_spell")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p2_spell", Notation, "receive_p2_spell")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p1_spell", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p1_spell")
	get_node("CanvasLayer/Deck Edit/WindowDialog").connect("send_p2_spell", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p2_spell")
	
	#telling what pieces players have
	Summoning.connect("update_p1_container", get_node("Context Menu/Menu"), "update_p1_container")
	Summoning.connect("update_p2_container", get_node("Context Menu/Menu"), "update_p2_container")
	Summoning.connect("update_p1_mp", get_node("Context Menu/Menu"), "update_p1_mp")
	Summoning.connect("update_p2_mp", get_node("Context Menu/Menu"), "update_p2_mp")
	Summoning.connect("update_p1_container", Notation, "update_p1_container")
	Summoning.connect("update_p2_container", Notation, "update_p2_container")
	Summoning.connect("update_p1_mp", Notation, "update_p1_mp")
	Summoning.connect("update_p2_mp",Notation, "update_p2_mp")
	
	#movement permission 
	Movement.connect("ask_p1", get_node("TurnQueue"), "send_p1")
	Movement.connect("ask_p2", get_node("TurnQueue"), "send_p2")
	Notation.connect("ask_p1", get_node("TurnQueue"), "send_p1")
	Notation.connect("ask_p2", get_node("TurnQueue"), "send_p2")
	get_node("TurnQueue").connect("send_player", Movement, "assign_player")
	get_node("TurnQueue").connect("send_player", Notation, "assign_player")
	
	#spell card effects and info 
	get_node("Context Menu/Menu").connect("reset_pre_effect", get_node("Spell Card/Premonition"), "effect_reset")
	get_node("Context Menu/Menu").connect("pre_effect2", get_node("Spell Card/Premonition"), "effect2")
	Movement.connect("reset_pre_effect", get_node("Spell Card/Premonition"), "effect_reset")

	Sacrifice.connect("reg_validate", get_node("Spell Card/Regicide"), "effect2")

	Movement.connect("fel_effect2", get_node("Spell Card/Field of Blood"), "effect2")
	Movement.connect("reset_fel", get_node("Spell Card/Field of Blood"), "effect_reset")
	Summoning.connect("reset_fel", get_node("Spell Card/Field of Blood"), "effect_reset")
	
		#soul swap info
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("request_info", get_node("Context Menu/Menu"), "send_soul_info")
	get_node("Context Menu/Menu").connect("soul_send", get_node("CanvasLayer2/Soul Swap/WindowDialog2"), "assign_info")
	Summoning.connect("update_p1_container", get_node("CanvasLayer2/Soul Swap/WindowDialog2"), "update_p1_container")
	Summoning.connect("update_p2_container", get_node("CanvasLayer2/Soul Swap/WindowDialog2"), "update_p2_container")
			#dialog sending new info
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("send_p1_info", get_node("Context Menu/Menu"), "receive_p1")
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("send_p2_info", get_node("Context Menu/Menu"), "receive_p2")
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("send_p1_info", Notation, "receive_p1")
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("send_p2_info", Notation, "receive_p2")
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("send_p1_info", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p1")
	get_node("CanvasLayer2/Soul Swap/WindowDialog2").connect("send_p2_info", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p2")
			#notation sneding new info
	Notation.connect("send_p1_info", get_node("Context Menu/Menu"), "receive_p1")
	Notation.connect("send_p2_info", get_node("Context Menu/Menu"), "receive_p2")
	Notation.connect("send_p1_info", Notation, "receive_p1")
	Notation.connect("send_p2_info", Notation, "receive_p2")
	Notation.connect("send_p1_info", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p1")
	Notation.connect("send_p2_info", get_node("CanvasLayer3/Deck View/WindowDialog3"), "receive_p2")


	Movement.connect("send_p1_rest", get_node("Context Menu/Menu"), "get_p1_rest")
	Movement.connect("send_p2_rest", get_node("Context Menu/Menu"), "get_p2_rest")
	Movement.connect("send_p1_rest", Notation, "get_p1_rest")
	Movement.connect("send_p2_rest", Notation, "get_p2_rest")
	
	#notation specific 
	Notation.connect("request_cell", get_node("Board"), "send_cell")
	Notation.connect("request_cell2", get_node("Board"), "send_cell2")
	get_node("Board").connect("send_cell", Notation, "receive_cell")
	get_node("Board").connect("send_cell2", Notation, "receive_cell2")
	Notation.connect("spell_use", get_node("Spell Card"), "ninput")
	Notation.connect("ss_use", get_node("Spell Card"), "nss_use")
	
	Notation.connect("send_output", get_node("Notation Container/Input"), "nprint")

	#reseting for new game
	NewGame.connect("reset", get_node("TurnQueue"), "reset")
	NewGame.connect("reset", Summoning, "reset")
	NewGame.connect("reset", get_node("Spell Card"), "reset")
	NewGame.connect("reset", Sacrifice, "reset")
	NewGame.connect("reset", get_node("Options/Options Container"), "reset")
	NewGame.connect("reset", Notation, "reset")
	NewGame.connect("reset", Nosum, "reset")
	NewGame.connect("reset", Movement, "reset")
	NewGame.connect("reset", get_node("Context Menu/Menu"), "reset")
	NewGame.connect("reset", get_node("TurnQueue"), "reset")
	NewGame.connect("reset", get_node("Notation Container/Input"), "reset")
	NewGame.connect("reset", get_node("Field View"), "reset")
	NewGame.connect("reset", get_node("Board"), "reset")
	
	NewGame.connect("board_enable", get_node("Mouse Script"), "board_enable")
