extends LineEdit

func _ready():
	set_process(true)

func _input(event):
	if event.is_action_released("ui_enter") and get_parent().visible == true:
		
		if "(" in self.text:
			self.text = self.text.replace("(", "[")
			self.text = self.text.replace(")", "]")
			
		var text_pre_deck = parse_json(self.text)
		#print(typeof(text_pre_deck))
		if text_pre_deck != null and typeof(text_pre_deck) == 19:
			if text_pre_deck.size() == 2 and typeof(text_pre_deck[1]) == 3:
				get_parent().pre_deck = text_pre_deck[0]
				get_parent().pre_spell = text_pre_deck[1]
				get_parent().confirm_check()

func ninput(input):
	var text_pre_deck = parse_json(input)
	#print(typeof(text_pre_deck))
	if text_pre_deck != null and typeof(text_pre_deck) == 19:
		if text_pre_deck.size() == 2 and typeof(text_pre_deck[1]) == 3:
			get_parent().pre_deck = text_pre_deck[0]
			get_parent().pre_spell = text_pre_deck[1]
			get_parent().confirm_check()
