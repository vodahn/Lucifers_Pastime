extends RayCast2D

var p1_perp = Vector2(105, 55) 
var p2_perp = Vector2(-95.38872,-56.55)
var chosen_perp = p1_perp

var mov_block 

signal turn_done

signal get_initial_cell
signal get_final_cell

signal try_send(trys)
signal mov_hide

signal ask_p1
signal ask_p2

signal send_p1_rest(type)
signal send_p2_rest(type)
signal reset_pre_effect(active_player)
signal fel_effect2(active_player, capd, capr) #captured piece type, id of "captor"
signal reset_fel(active_player)

var moving_piece

var p1_container
var p2_container

var initial_cell = null
var final_cell = null

var active_player 
var turn_count = 0 

var preemptive = false
var preic = null
var prefc = null
var pre_piece = null

var cap_stat = false

var p1_prem_effect = 0
var p2_prem_effect = 0
var prem_summoned
var p1_res_effect = 0
var p2_res_effect = 0
var p1_res_piece = null
var p2_res_piece = null
var p1_fel_effect = 0
var p2_fel_effect = 0
var p1_fel_capd = null
var p2_fel_capd = null
var p1_fel_capr = null
var p2_fel_capr = null

func movement_begin(input_piece):
	moving_piece = input_piece
	emit_signal("get_initial_cell")
	
	if mov_block == true:
		return
	try_calc() #for the sake of preemptive movement

	
	
func movement_end():
	emit_signal("get_final_cell")
	
func assign_initial_cell(current_cell):
	initial_cell = current_cell

func assign_final_cell(current_cell):
	final_cell = current_cell
	valid_check()

# I should probably have three valid_checks, one for null checking, one for piece specific, and one for general rules

#null check
func valid_check():
	if initial_cell == null:
		return
	if moving_piece == null:
		return
	
	var p_initial_cell = initial_cell #memory protection? I don't know if this is necessary, but it seems to help
	var p_final_cell = final_cell
		
	if	final_cell == initial_cell or mov_block == true:
		if preemptive == true:
			preemptive = false
			preic = null
			prefc = null
			pre_piece = null
		move_failed(p_initial_cell)
		return
		
	var moving_player
	var other_player
	if "p1" in moving_piece.id:
			moving_player = "p1"
			other_player = "p2"
	else:
		moving_player = "p2"
		other_player = "p1"
	
	if final_cell != initial_cell:
		if final_cell == null:
			move_failed(p_initial_cell)
		else:
			valid_check2(p_initial_cell, p_final_cell, moving_player, other_player)

#piece specific rules
func valid_check2(p_initial_cell, p_final_cell, moving_player, other_player): 
	var path_check = null
	
	if (active_player.name in moving_piece.name) == false: #correct active player check
			#this should be done earlier, patha and stuff can mess it up
			preemptive = true
			preic = p_initial_cell
			prefc = p_final_cell
			pre_piece = moving_piece

			move_failed(p_initial_cell)
			return

	if ((active_player.name == "p1" and p1_prem_effect == 2) or (active_player.name == "p2" and p2_prem_effect == 2)):
		if moving_piece.id != prem_summoned:
			move_failed(p_initial_cell)
			return
		
	if ((active_player.name == "p1" and p1_prem_effect != 2) or (active_player.name == "p2" and p2_prem_effect != 2)):
		if moving_piece.lre != null:
			if int(final_cell.coordinate.y - initial_cell.coordinate.y) % 2 == 0 : #even change in y
				for i in moving_piece.lre:
					if path_check != true:
						path_check = lre_check(i)
					
		if moving_piece.lro != null and path_check != true:
			for i in moving_piece.lro:
				if path_check != true:
					path_check = lro_check(i)
	# first check if y change is even or odd, then send appropirately to two functions
	#in lre_check, check if initial row is even or odd and use appropriate try for that 
		
	if moving_piece.path_relations != null and path_check != true:
		for i in moving_piece.path_relations:
			#you need some way of checking if a path is blocked
			if abs(final_cell.coordinate.x - initial_cell.coordinate.x) <= i.x:
				if i.y == 1 and path_check != true:
					path_check = dir1(i, moving_player) #not sure why moving player is passed/forgot
				elif i.y == 4 and path_check != true:
					path_check = dir4(i, moving_player)
					
			if abs(final_cell.coordinate.y - initial_cell.coordinate.y) <= i.x:
				if i.y == 2 and path_check != true:
					path_check = dir2(i, moving_player)
				elif i.y == 3 and path_check != true:
					path_check = dir3(i, moving_player)
				elif i.y == 5 and path_check != true:
					path_check = dir5(i, moving_player)
				elif i.y == 6 and path_check != true:
					path_check = dir6(i, moving_player)
	if path_check == true:
		valid_check3(p_initial_cell, p_final_cell, moving_player, other_player)
	else:
		move_failed(p_initial_cell)

#general rules
func valid_check3(p_initial_cell, p_final_cell, moving_player, other_player):
	if final_cell.occupying == null:
		move()
			
	else: #if final cell is occupied
				
		var occupying_piece
				
				#this sort of iteration will probably how id is used everywhere. 
				#Not sure about efficiency, but seems to work
		if "p1" in p_final_cell.occupying:
			for i in p1_container.get_children():
				if i.id == p_final_cell.occupying:
					occupying_piece = i

						
		if "p2" in p_final_cell.occupying:
			for i in p2_container.get_children():
				if i.id == p_final_cell.occupying:
					occupying_piece = i

		#piece owner is same as moved one 
		if moving_player in occupying_piece.id or (active_player.name in moving_piece.name) == false: 
			move_failed2(occupying_piece, p_initial_cell, p_final_cell) 
			return
		#capturing 
		else: #different owners
			Nosum.remove_nsz(occupying_piece)
			Release.release(occupying_piece, other_player)
			
			if (other_player == "p1" and p1_res_effect == 1):
				p1_res_piece = occupying_piece.piece_type
				Summoning.p1_res_piece = occupying_piece.piece_type
				emit_signal("send_p1_rest", p1_res_piece)
			if (other_player == "p2" and p2_res_effect == 1):
				p2_res_piece = occupying_piece.piece_type
				Summoning.p2_res_piece = occupying_piece.piece_type
				emit_signal("send_p2_rest", p2_res_piece)

			if (other_player == "p2" and p1_fel_effect == 1):
				p1_fel_capd = occupying_piece.piece_type
				p1_fel_capr = moving_piece.id
				emit_signal("fel_effect2", active_player.name, p1_fel_capd, p1_fel_capr)
			if (other_player == "p1" and p2_fel_effect == 1):
				p2_fel_capd = occupying_piece.piece_type
				p2_fel_capr = moving_piece.id
				emit_signal("fel_effect2", active_player.name, p2_fel_capd, p2_fel_capr)

			#add reset when fel effect is 2 and occupying piece's id is equal to capr
			if (other_player == "p1" and p1_fel_effect == 2):
				if occupying_piece.id == p1_fel_capr:
					emit_signal("reset_fel", "p1")
			if (other_player == "p2" and p2_fel_effect == 2):
				if occupying_piece.id == p2_fel_capr:
					emit_signal("reset_fel", "p2")
			
			if occupying_piece == pre_piece:
				preemptive = false
				preic = null
				prefc = null
				pre_piece = null

			occupying_piece.queue_free()
			yield(get_tree().create_timer(.1), "timeout")
			cap_stat = true
			move()

func move():
	moving_piece.global_position = final_cell.global_position + chosen_perp
	initial_cell.set_occupying(null)
	final_cell.set_occupying(moving_piece.id)
	moving_piece.cur_coor = final_cell.coordinate
	try_hide()
	Nosum.remove_nsz(moving_piece)
	Nosum.nsz_calc(active_player, final_cell, moving_piece)
	Notation.mov_print(moving_piece.piece_type, initial_cell.name, final_cell.name, cap_stat)
	initial_cell = null
	final_cell = null

	if ((active_player.name == "p1" and p1_prem_effect == 2) or (active_player.name == "p2" and p2_prem_effect == 2)):
		emit_signal("reset_pre_effect", active_player.name)
	cap_stat = false
	emit_signal("turn_done")

func move_failed(p_initial_cell): #if final cell is null(inbetween cells) or non-active player moves
	moving_piece.global_position = p_initial_cell.global_position + chosen_perp
	initial_cell = null
	final_cell = null
	try_hide()

func move_failed2(occupying_piece, p_initial_cell, p_final_cell): #if occupying piece is player's or non-active player moves
	moving_piece.dragging = false 
	occupying_piece.dragging = false
	moving_piece.global_position = p_initial_cell.global_position + chosen_perp
	occupying_piece.global_position = p_final_cell.global_position  + chosen_perp
	initial_cell = null
	final_cell = null
	try_hide()


#leap checks
func lre_check(i):
	var path_check #not really a path, but I don't feel like changing the var names 
	var try = initial_cell.coordinate + i
	if try == final_cell.coordinate:
		path_check = true
	return path_check

func lro_check(i):
	var path_check
	var try
	if int(initial_cell.coordinate.y) % 2 == 0: #even initial row
		try = initial_cell.coordinate + Vector2(i[0][0], i[1])
	elif int(initial_cell.coordinate.y) % 2:
		try = initial_cell.coordinate + Vector2(i[0][1], i[1])
	
	if try == final_cell.coordinate:
		path_check = true
	return path_check
	
#path directions
func dir1(i, moving_player):
	var path_check
	var previous_cell = initial_cell.coordinate
	var blocking_piece
	
	for x in i.x:
		var try = Vector2(previous_cell.x - 1, previous_cell.y)
		if final_cell.coordinate != try:
			
			if p1_container != null:
				for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
				if blocking_piece != null and blocking_piece.cur_coor.x > final_cell.coordinate.x:
					path_check = false
					break
					
			if p2_container != null:
				for y in p2_container.get_children():
					if y.cur_coor == try:
						blocking_piece = y
				if blocking_piece != null and blocking_piece.cur_coor.x > final_cell.coordinate.x:
					path_check = false
					break
				
			path_check = false
			previous_cell = try
		else: 
			path_check = true
	return path_check
				
func dir2(i, moving_player):
	var path_check
	var previous_cell = initial_cell.coordinate
	var blocking_piece
	
	for x in i.x:
		if int(previous_cell.y) % 2: #if odd
			var try = Vector2(previous_cell.x - 1, previous_cell.y + 1)
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
					
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
		else: 
			var try = Vector2(previous_cell.x, previous_cell.y + 1)
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
					
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
	return path_check
					
func dir3(i, moving_player):
	var path_check
	var previous_cell = initial_cell.coordinate
	var blocking_piece
	
	for x in i.x:
		if int(previous_cell.y) % 2:
			var try = Vector2(previous_cell.x, previous_cell.y + 1)
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
				
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
		else: 
			var try = Vector2(previous_cell.x + 1, previous_cell.y + 1)
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
				
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y < final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
	return path_check
	
func dir4(i, moving_player):
	var path_check
	var previous_cell = initial_cell.coordinate
	var blocking_piece
	
	for x in i.x:
		var try = Vector2(previous_cell.x + 1, previous_cell.y)
		if final_cell.coordinate != try:
			
			if p1_container != null:
				for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
				if blocking_piece != null and blocking_piece.cur_coor.x < final_cell.coordinate.x:
					path_check = false
					break
			
			if p2_container != null:
				for y in p2_container.get_children():
					if y.cur_coor == try:
						blocking_piece = y
				if blocking_piece != null and blocking_piece.cur_coor.x < final_cell.coordinate.x:
					path_check = false
					break
				
			path_check = false
			previous_cell = try
		else: 
			path_check = true
			break
	return path_check
	
func dir5(i, moving_player):
	var path_check
	var previous_cell = initial_cell.coordinate
	var blocking_piece
	
	for x in i.x:
		if int(previous_cell.y) % 2:
			var try = Vector2(previous_cell.x, previous_cell.y - 1)
			
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
					
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
		else: 
			var try = Vector2(previous_cell.x + 1, previous_cell.y - 1)
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
				
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
	return path_check
	
func dir6(i, moving_player):
	var path_check
	var previous_cell = initial_cell.coordinate
	var blocking_piece
	
	for x in i.x:
		if int(previous_cell.y) % 2:
			var try = Vector2(previous_cell.x - 1, previous_cell.y - 1)

			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
				
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
					
				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
		else: 
			var try = Vector2(previous_cell.x, previous_cell.y - 1)
			if final_cell.coordinate != try:
				
				if p1_container != null:
					for y in p1_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break
				
				if p2_container != null:
					for y in p2_container.get_children():
						if y.cur_coor == try:
							blocking_piece = y
					if blocking_piece != null and blocking_piece.cur_coor.y > final_cell.coordinate.y:
						path_check = false
						break

				path_check = false
				previous_cell = try
			else: 
				path_check = true
				break
	return path_check

	
func update_p1_container(new_container):
	p1_container = new_container
func update_p2_container(new_container):
	p2_container = new_container

func try_calc(): #activated as soon as piece picked up, shows possible movements 
	var ctrys = []
	var movers_container
	var opposing_container
	
	if "p1" in moving_piece.name:
		movers_container = p1_container
		opposing_container = p2_container
	if "p2" in moving_piece.name:
		movers_container = p2_container
		opposing_container = p1_container

	#leap model
	if ((active_player.name == "p1" and p1_prem_effect != 2) or (active_player.name == "p2" and p2_prem_effect != 2)):
		if moving_piece.lre != null:
			for i in moving_piece.lre:
				var try = initial_cell.coordinate + i
				
				#leap's can only be blocked by their own pieces; no breaking
				var check = true 
				for j in movers_container.get_children():
					if j.cur_coor == try:
						check = false
				if check == true:
					ctrys.append(try)

		if moving_piece.lro != null:
			for i in moving_piece.lro:
				var try
				if int(initial_cell.coordinate.y) % 2 == 0: #even initial row
					try = initial_cell.coordinate + Vector2(i[0][0], i[1])
				elif int(initial_cell.coordinate.y) % 2:
					try = initial_cell.coordinate + Vector2(i[0][1], i[1])
				
				var check = true
				for j in movers_container.get_children():
					if j.cur_coor == try:
						check = false
				if check == true:
					ctrys.append(try)
	
	#path model
	if moving_piece.path_relations != null: 
		for i in moving_piece.path_relations:
			
			if i.y == 1:
				var previous_cell = initial_cell.coordinate
				for x in i.x:
					var try = Vector2(previous_cell.x - 1, previous_cell.y)

					#insert check
					var check = true
					for j in movers_container.get_children():
						if j.cur_coor == try:
							check = false
					if check == true:
						ctrys.append(try)
					else:
						break #hopefully wont break path_relations loop

					var check2 = true
					for j in opposing_container.get_children():
						if j.cur_coor == try:
							check2 = false
					if check2 == true:
						ctrys.append(try)
					else:
						ctrys.append(try) #still added
						break 

					previous_cell = try

			if i.y == 4:
				var previous_cell = initial_cell.coordinate
				for x in i.x:
					var try = Vector2(previous_cell.x + 1, previous_cell.y)

					var check = true
					for j in movers_container.get_children():
						if j.cur_coor == try:
							check = false
					if check == true:
						ctrys.append(try)
					else:
						break 

					var check2 = true
					for j in opposing_container.get_children():
						if j.cur_coor == try:
							check2 = false
					if check2 == true:
						ctrys.append(try)
					else:
						ctrys.append(try) 
						break 

					previous_cell = try

			if i.y == 2:
				var previous_cell = initial_cell.coordinate
				for x in i.x:
					if int(previous_cell.y) % 2: #if odd
						var try = Vector2(previous_cell.x - 1, previous_cell.y + 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try
					else: 
						var try = Vector2(previous_cell.x, previous_cell.y + 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try

			if i.y == 3:
				var previous_cell = initial_cell.coordinate
				for x in i.x:
					if int(previous_cell.y) % 2: #if odd
						var try = Vector2(previous_cell.x, previous_cell.y + 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try
					else: 
						var try = Vector2(previous_cell.x + 1, previous_cell.y + 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try

			if i.y == 5:
				var previous_cell = initial_cell.coordinate
				for x in i.x:
					if int(previous_cell.y) % 2: #if odd
						var try = Vector2(previous_cell.x, previous_cell.y - 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try
					else: 
						var try = Vector2(previous_cell.x + 1, previous_cell.y - 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try

			if i.y == 6:
				var previous_cell = initial_cell.coordinate
				for x in i.x:
					if int(previous_cell.y) % 2: #if odd
						var try = Vector2(previous_cell.x - 1, previous_cell.y - 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try
					else: 
						var try = Vector2(previous_cell.x, previous_cell.y - 1)

						var check = true
						for j in movers_container.get_children():
							if j.cur_coor == try:
								check = false
						if check == true:
							ctrys.append(try)
						else:
							break 

						var check2 = true
						for j in opposing_container.get_children():
							if j.cur_coor == try:
								check2 = false
						if check2 == true:
							ctrys.append(try)
						else:
							ctrys.append(try) 
							break 

						previous_cell = try
						
	emit_signal("try_send", ctrys)
	
func try_hide():
	emit_signal("mov_hide")

func block_move():
	mov_block = true
func unblock_move():
	mov_block = false

func set_turn_count():
	turn_count += 1
	
	if turn_count % 2:
		get_p1()
	else:
		get_p2()

	if ((turn_count < 3) or 
		(p1_prem_effect == 1 and active_player.name == "p1") or (p2_prem_effect == 1 and active_player.name == "p2")) :
		mov_block = true
	else:
		mov_block = false

	if preemptive == true:
		yield(get_tree().create_timer(.1), "timeout")
		initial_cell = preic
		final_cell = prefc
		moving_piece = pre_piece
		preemptive = false
		preic = null
		prefc = null
		pre_piece = null
		valid_check()
	
func get_p1():
	emit_signal("ask_p1")
func get_p2():
	emit_signal("ask_p1")
func assign_player(player):
	active_player = player

func change_perp(player):
	if player == "p1":
		chosen_perp = p1_perp
	else:
		chosen_perp = p2_perp


func reset():
	mov_block = null

	moving_piece = null

	p1_container = []
	p2_container = []

	initial_cell = null
	final_cell = null

	active_player 
	turn_count = 0 

	preemptive = false
	preic = null
	prefc = null
	pre_piece = null

	cap_stat = false

	p1_prem_effect = 0
	p2_prem_effect = 0
	prem_summoned = null
	p1_res_effect = 0
	p2_res_effect = 0
	p1_res_piece = null
	p2_res_piece = null
	p1_fel_effect = 0
	p2_fel_effect = 0
	p1_fel_capd = null
	p2_fel_capd = null
	p1_fel_capr = null
	p2_fel_capr = null
