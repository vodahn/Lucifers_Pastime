extends WindowDialog


func _on_p1_button_pressed():
	if get_parent().get_parent().get_parent().get_node("Board").mode == "p2":
		get_parent().get_parent().get_parent().get_node("Board").change_mode("p1")
	Movement.change_perp("p1")
	Summoning.change_perp("p1")


func _on_p2_button_pressed():
	if get_parent().get_parent().get_parent().get_node("Board").mode == "p1":
		get_parent().get_parent().get_parent().get_node("Board").change_mode("p2")
	Movement.change_perp("p2")
	Summoning.change_perp("p2")


func _on_WindowDialog4_popup_hide():
	get_parent().get_parent().get_parent().get_node("Board").mode_set()
