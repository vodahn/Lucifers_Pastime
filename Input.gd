extends LineEdit

var turn_count = 0
var printing_turn = 0

var open = "[url]"
var close = "[/url]"

var input_disable = false

var p1_deck_ouput
var p2_deck_ouput

signal being_chosen(player)
signal deck_input(text)

func _ready():
	self.modulate.a = 0.3
	set_process(true)

func _input(event):
	if event.is_action_released("ui_enter"):
		if input_disable == true:
			self.text = ""
			return
		if turn_count != 0:
			if "︵ ┻━┻" in self.text:
				if turn_count % 2 == 0:
					get_parent().get_parent().get_node("TurnQueue").won_game("p1f")
				else:
					get_parent().get_parent().get_node("TurnQueue").won_game("p2f")
				self.text = ""
			else:
				Notation.comm_split(self.text)
		else:
			if "p1:" in self.text:
				var sent = self.text.split(":")
				emit_signal("being_chosen", "p1")
				emit_signal("deck_input", sent[1])
			elif "p2:" in self.text:
				var sent = self.text.split(":")
				emit_signal("being_chosen", "p2")
				emit_signal("deck_input", sent[1])
		self.text = ""


func nprint(output):

	if turn_count == 0:
		if get_parent().get_node("Notation Box").bbcode_text == "":
			get_parent().get_node("Notation Box").bbcode_text += open + output +close
		else:
			get_parent().get_node("Notation Box").bbcode_text += "\n" + open + output + close
	elif printing_turn != turn_count:
		get_parent().get_node("Notation Box").bbcode_text += ("\n\n" + str(turn_count) + 
				"." + "\t" + open + output)
		printing_turn = turn_count
	elif "'" in output:
		get_parent().get_node("Notation Box").bbcode_text += output
	else:
		get_parent().get_node("Notation Box").bbcode_text += (", " + output)
		
	if "p1:" in output:
		p1_deck_ouput = output
	if "p2:" in output:
		p2_deck_ouput = output

func set_turn_count():
	if turn_count > 0:
		get_parent().get_node("Notation Box").bbcode_text += close
	turn_count += 1
	
	if turn_count == 1:
		get_parent().get_node("Notation Box").bbcode_text = (open + p1_deck_ouput + close +
		 "\n" + open + p2_deck_ouput + close)

func _on_Mouse_Script_te_deselect():
	if self.has_focus() == true:
		self.release_focus()

func disable_input():
	input_disable = true

func reset():
	turn_count = 0
	printing_turn = 0
	
	input_disable = false

func won():
	turn_count = 0
