extends RayCast2D

var active_player
var cell
var show_cell = false

var board_disable


#signal turn_done
signal menu_show(player, picked_cell)
signal menu_hide
signal te_deselect

signal send_icell(current_cell)
signal send_fcell(current_cell)

func _ready(): 
	set_process_input(true)
	set_process(true)
	
func _physics_process(_delta):
		# get the Physics2DDirectSpaceState object
	var space = get_world_2d().direct_space_state
	# Get the mouse's position
	var mousePos = get_global_mouse_position()
	# Check if there is a collision at the mouse position
	if space.intersect_point(mousePos, 1, [], 2147483647, true, true):
		cell = space.intersect_point(mousePos, 1, [], 2147483647, true, true)[0].collider
	else:
		cell = null
	
	if cell != null:
		if "p1" in cell.name or "p2" in cell.name: #intersect or something makes cell = piece if inbetween cells fsr
			cell = null
	
	if cell != null and show_cell == true:
		get_parent().get_node("cell label").text = cell.name

	
func _input(event):
	if event.is_action_pressed("left_click"):
		emit_signal("te_deselect")
		
	if board_disable == true:
		return
	
	if event.is_action_pressed("right_click") and cell != null:
		emit_signal("menu_show", active_player, cell)
	if event.is_action_pressed("right_click") and cell == null:
		emit_signal("menu_hide")
	if event.is_action_pressed("left_click"):
		yield(get_tree().create_timer(.1), "timeout")
		emit_signal("menu_hide")
		
func _on_p1_mouse_control(player):
	active_player = player
	
func _on_p2_mouse_control(player):
	active_player = player
	

func send_initial_cell():
	emit_signal("send_icell", cell)

func send_final_cell():
	emit_signal("send_fcell", cell)

func _on_Board_disable_board():
	board_disable = true

func board_enable():
	board_disable = false
