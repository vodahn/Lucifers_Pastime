extends Node

func pre_effect(active_player):
	if active_player == "p1":
		Summoning.p1_res_effect = 1
		Movement.p1_res_effect = 1
		Notation.p1_res_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p1_res_effect = 1
	else:
		Summoning.p2_res_effect = 1
		Movement.p2_res_effect = 1
		Notation.p2_res_effect = 1
		get_parent().get_parent().get_node("Context Menu/Menu").p2_res_effect = 1

func effect(active_player):
	if active_player == "p1":
		Summoning.p1_res_effect = 2
		Movement.p1_res_effect = 2
		Notation.p1_res_effect = 2
		get_parent().get_parent().get_node("Context Menu/Menu").p1_res_effect = 2
		get_parent().p1_res_turn = get_parent().turn_count
	else:
		Summoning.p2_res_effect = 2
		Movement.p2_res_effect = 2
		Notation.p2_res_effect = 2
		get_parent().get_parent().get_node("Context Menu/Menu").p2_res_effect = 2
		get_parent().p2_res_turn = get_parent().turn_count
	get_parent().end_turn()
	yield()

func effect_reset(active_player):
	if active_player == "p1":
		Summoning.p1_res_effect = 0
		Movement.p1_res_effect = 0
		Notation.p1_res_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p1_res_effect = 0
	else:
		Summoning.p2_res_effect = 0
		Movement.p2_res_effect = 0
		Notation.p2_res_effect = 0
		get_parent().get_parent().get_node("Context Menu/Menu").p2_res_effect = 0
