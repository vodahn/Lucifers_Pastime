extends WindowDialog

var active_player
var p1_deck
var p2_deck
var p1_container
var p2_container 

var p_pressed = null #player's pressed button
var op_pressed = null #other player's pressed button

signal request_info
signal send_p1_info(np1_deck)
signal send_p2_info(np2_deck)

func _ready():
	for i in get_children():
		if "Sprite" in i.name:
			i.visible = false

func _on_WindowDialog2_about_to_show():
	emit_signal("request_info")

func assign_info(ap, deck1, deck2):
	active_player = ap
	p1_deck = deck1
	p2_deck = deck2
	assign_buttons()

func assign_buttons():
	var top
	var bottom
	if active_player == "p1":
		top = "p1 top"
		bottom = "p2 bottom"
	else:
		top = "p2 top"
		bottom = "p1 bottom"
	assign_buttons2(top)
	assign_buttons2(bottom)

func assign_buttons2(row):
	var deck
	var buttons
	if "p1" in row:
		deck = p1_deck
	else:
		deck = p2_deck
	if "top" in row:
		buttons = [get_node("1"), get_node("2"), get_node("3"), get_node("4"), get_node("5")]
	else:
		buttons = [get_node("6"), get_node("7"), get_node("8"), get_node("9"),get_node("10")]
	
	var x = 0
	for i in deck:
		if i != 16:
			buttons[x].deck_number = i
			x += 1
	assign_buttons3(buttons, row)
	
func assign_buttons3(buttons, row):
	#another variable in each button script may be needed to assign which sprite is assigned to it. This variable will become
	#visible and hidden accordingly. Text will also be assigned here
	for i in buttons:
		if i.deck_number == 0:
			i.sprite_node = get_node("Apprentice Sprite")
			i.cost = 2
			i.piece_type = "Apprentice"
			i.text = "A"
		if i.deck_number == 1:
			i.sprite_node = get_node("Iron Maiden Sprite")
			i.cost = 2
			i.piece_type = "Iron Maiden"
			i.text = "I"
		if i.deck_number == 2:
			i.sprite_node = get_node("IttanMomen Sprite")
			i.cost = 2
			i.piece_type = "IttanMomen"
			i.text = "It"
		if i.deck_number == 3:
			i.sprite_node = get_node("Nekomata Sprite")
			i.cost = 2
			i.piece_type = "Nekomata"
			i.text = "N"
		if i.deck_number == 4:
			i.sprite_node = get_node("Harpy Sprite")
			i.cost = 10
			i.piece_type = "Harpy"
			i.text = "H"
		if i.deck_number == 5:
			i.sprite_node = get_node("Redcap Sprite")
			i.cost = 10
			i.piece_type = "Redcap"
			i.text = "Rc"
		if i.deck_number == 6:
			i.sprite_node = get_node("Slime Sprite")
			i.cost = 10
			i.piece_type = "Slime"
			i.text = "S"
		if i.deck_number == 7:
			i.sprite_node = get_node("Holstaur Sprite")
			i.cost = 10
			i.piece_type = "Holstaur"
			i.text = "Hs"
		if i.deck_number == 8:
			i.sprite_node = get_node("Red Oni Sprite")
			i.cost = 10
			i.piece_type = "Red Oni"
			i.text = "Ro"
		if i.deck_number == 9:
			i.sprite_node = get_node("Blue Oni Sprite")
			i.cost = 10
			i.piece_type = "Blue Oni"
			i.text = "B"
		if i.deck_number == 10:
			i.sprite_node = get_node("Priestess Sprite")
			i.cost = 20
			i.piece_type = "Priestess"
			i.text = "P"
		if i.deck_number == 11:
			i.sprite_node = get_node("Imp Sprite")
			i.cost = 20
			i.piece_type = "Imp"
			i.text = "Im"
		if i.deck_number == 12:
			i.sprite_node = get_node("False Angel Sprite")
			i.cost = 1
			i.piece_type = "False Angel"
			i.text = "F"
		if i.deck_number == 13:
			i.sprite_node = get_node("Queen Slime Sprite")
			i.cost = 50
			i.piece_type = "Queen Slime"
			i.text = "Qs"
		if i.deck_number == 14:
			i.sprite_node = get_node("Automaton Sprite")
			i.cost = 50
			i.piece_type = "Automaton"
			i.text = "Au"
		if i.deck_number == 15:
			i.sprite_node = get_node("Sylph Sprite")
			i.cost = 50
			i.piece_type = "Sylph"
			i.text = "Sy"
	button_check(row)

#will check which buttons should be disabled based on spell's limiting conditions 
func button_check(row):
	var container
	var buttons 
	
	if "bottom" in row:
		buttons = [get_node("6"), get_node("7"), get_node("8"), get_node("9"),get_node("10")]
	else:
	# active player is always on top and their pieces don't have to be other board for their card to be swapped
		return
	
	if active_player == "p1":
		container = p2_container
	else:
		container = p1_container
	#checking the opposite player's buttons and rows
	#if they have said piece type on the board 
	for i in buttons:
		var check = false
		for x in container.get_children():
			if i.piece_type == x.piece_type:
				check = true
		if check == false:
			i.disabled = true

#will check if a button stays down or not(equal cost between exchanged pieces
#activated when button is pressed, receives the one pressed
func button_check2(button):
	#if a player's button was already pressed 
	if p_pressed != null:
		if button.name in ["1", "2", "3", "4", "5"]:

			#if toggling
			if p_pressed.piece_type == button.piece_type:
				p_pressed = null
				return

			for i in [get_node("1"), get_node("2"), get_node("3"), get_node("4"), get_node("5")]:
				#unpressed other buttons
				if button.name != i.name:
					i.pressed = false

	if op_pressed != null:
		if button.name in ["6", "7", "8", "9", "10"]:
			
			if op_pressed.piece_type == button.piece_type:
				op_pressed = null
				return

			for i in [get_node("6"), get_node("7"), get_node("8"), get_node("9"), get_node("10")]:
				if button.name != i.name:
					i.pressed = false
					
	#cost and dupliacte checks
	#other player's button always gets unpressed if these  fail, yours never does
	if button.name in ["1", "2", "3", "4", "5"]: #button is user's
		p_pressed = button

		#duplicate check
		for i in [get_node("6"), get_node("7"), get_node("8"), get_node("9"), get_node("10")]:
			if p_pressed.piece_type == i.piece_type:
				p_pressed.pressed = false
				p_pressed = null
				return

		if op_pressed != null:
			if p_pressed.cost != op_pressed.cost:
				op_pressed.pressed = false
				op_pressed = null
		
	else: #button is not user's
		op_pressed = button
		
		for i in [get_node("1"), get_node("2"), get_node("3"), get_node("4"), get_node("5")]:
			if op_pressed.piece_type == i.piece_type:
				op_pressed.pressed = false
				op_pressed = null
				return

		if p_pressed != null:
			if p_pressed.cost != op_pressed.cost:
				op_pressed.pressed = false
				op_pressed = null

#will be performed when confirm is pressed, not subject to any other logic. Failed button presses don't change the deck
func send_new_decks():
	emit_signal("send_p1_info", p1_deck)
	emit_signal("send_p2_info", p2_deck)


func _on_Confirm_Button_pressed():
#will look at which buttons are pressed instead of p_pressed and op_pressed for logical simplicity 
	var check1 = false
	var check2 = false
	var button1
	var button2
	var deck1
	var deck2
	for i in [get_node("1"), get_node("2"), get_node("3"), get_node("4"), get_node("5")]:
		if i.pressed == true:
			check1 = true
			button1 = i
	for i in [get_node("6"), get_node("7"), get_node("8"), get_node("9"), get_node("10")]:
		if i.pressed == true:
			check2 = true
			button2 = i
	
	#performing action
	if check1 == true and check2 == true:
		var dn1
		var dn2
		if active_player == "p1":
			deck1 = p1_deck
			deck2 = p2_deck
		else:
			deck1 = p2_deck
			deck2 = p1_deck
		for i in deck1:
			if i == button1.deck_number:
				dn1 = i
				deck1.erase(i)
		deck1.append(button2.deck_number)
		for i in deck2:
			if i == button2.deck_number:
				dn2 = i
				deck2.erase(i)
		deck2.append(button1.deck_number)
		
		Notation.diass_convert(dn1, dn2)
		send_new_decks()
		self.visible = false


func _on_1_mouse_entered():
	get_node("1").sprite_node.visible = true
func _on_1_mouse_exited():
	get_node("1").sprite_node.visible = false
func _on_2_mouse_entered():
	get_node("2").sprite_node.visible = true
func _on_2_mouse_exited():
	get_node("2").sprite_node.visible = false
func _on_3_mouse_entered():
	get_node("3").sprite_node.visible = true
func _on_3_mouse_exited():
	get_node("3").sprite_node.visible = false
func _on_4_mouse_entered():
	get_node("4").sprite_node.visible = true
func _on_4_mouse_exited():
	get_node("4").sprite_node.visible = false
func _on_5_mouse_entered():
	get_node("5").sprite_node.visible = true
func _on_5_mouse_exited():
	get_node("5").sprite_node.visible = false
func _on_6_mouse_entered():
	get_node("6").sprite_node.visible = true
func _on_6_mouse_exited():
	get_node("6").sprite_node.visible = false
func _on_7_mouse_entered():
	get_node("7").sprite_node.visible = true
func _on_7_mouse_exited():
	get_node("7").sprite_node.visible = false
func _on_8_mouse_entered():
	get_node("8").sprite_node.visible = true
func _on_8_mouse_exited():
	get_node("8").sprite_node.visible = false
func _on_9_mouse_entered():
	get_node("9").sprite_node.visible = true
func _on_9_mouse_exited():
	get_node("9").sprite_node.visible = false
func _on_10_mouse_entered():
	get_node("10").sprite_node.visible = true
func _on_10_mouse_exited():
	get_node("10").sprite_node.visible = false

func _on_1_pressed():
	button_check2(get_node("1"))
func _on_2_pressed():
	button_check2(get_node("2"))
func _on_3_pressed():
	button_check2(get_node("3"))
func _on_4_pressed():
	button_check2(get_node("4"))
func _on_5_pressed():
	button_check2(get_node("5"))
func _on_6_pressed():
	button_check2(get_node("6"))
func _on_7_pressed():
	button_check2(get_node("7"))
func _on_8_pressed():
	button_check2(get_node("8"))
func _on_9_pressed():
	button_check2(get_node("9"))
func _on_10_pressed():
	button_check2(get_node("10"))


func _on_WindowDialog2_popup_hide():
	var buttons = [get_node("6"), get_node("7"), get_node("8"), get_node("9"),get_node("10")]
	for i in buttons:
		i.disabled = false
	buttons = [get_node("1"), get_node("2"), get_node("3"), get_node("4"),get_node("5"), get_node("6"), get_node("7"), 
		get_node("8"), get_node("9"),get_node("10")]
	for i in buttons:
		i.pressed = false
	
	p_pressed = null
	op_pressed = null
	get_parent().get_parent().get_parent().get_node("Spell Card").end_turn()

func update_p1_container(new_container):
	p1_container = new_container
func update_p2_container(new_container):
	p2_container = new_container
