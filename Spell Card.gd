extends Node

var active_player
var turn_count = 0

signal turn_done

var p1_spell
var p2_spell

var p1_used = false
var p2_used = false

var p1_res_turn = 0
var p2_res_turn = 0
var p1_reg_turn = 0
var p2_reg_turn = 0

func _ready():
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("ui_spell") and turn_count > 2:
		if active_player == "p1":
			if p1_used == false:
				p1_used = true
				get_parent().get_node("Context Menu/Menu").hide_menu()
				Notation.nspell_use()
				Notation.spell_print()
				Notation.nspell_disable()
				p1_spell.effect(active_player)
		else:
			if p2_used == false:
				p2_used = true
				get_parent().get_node("Context Menu/Menu").hide_menu()
				Notation.nspell_use()
				Notation.spell_print()
				Notation.nspell_disable()
				p2_spell.effect(active_player)

func ninput():
	if active_player == "p1":
		if p1_used == false:
			p1_used = true
			get_parent().get_node("Context Menu/Menu").hide_menu()
			Notation.nspell_use()
			Notation.nspell_disable()
			p1_spell.effect(active_player)
	else:
		if p2_used == false:
			p2_used = true
			get_parent().get_node("Context Menu/Menu").hide_menu()
			Notation.nspell_use()
			Notation.nspell_disable()
			p2_spell.effect(active_player)
	Notation.spell_print()

func nss_use():
	if active_player == "p1":
		if p1_used == false:
			p1_used = true
			get_parent().get_node("Context Menu/Menu").hide_menu()
			Notation.nspell_use()
			Notation.nspell_disable()
	else:
		if p2_used == false:
			p2_used = true
			get_parent().get_node("Context Menu/Menu").hide_menu()
			Notation.nspell_use()
			Notation.nspell_disable()
	Notation.spell_print()

func set_turn_count():
	turn_count += 1
	if turn_count % 2:
		#these are strings
		active_player = "p1"
	else:
		active_player = "p2"
	if p1_res_turn != 0:
		if (turn_count - p1_res_turn) > 2:
			get_node("Resurrection").effect_reset("p1")
			p1_res_turn = 0
	if p2_res_turn != 0:
		if (turn_count - p2_res_turn) > 2:
			get_node("Resurrection").effect_reset("p2")
			p2_res_turn = 0
	if p1_reg_turn != 0:
		if (turn_count - p1_reg_turn) > 2:
			get_node("Regicide").effect_reset("p1")
			p1_reg_turn = 0
	if p2_reg_turn != 0:
		if (turn_count - p2_reg_turn) > 2:
			get_node("Regicide").effect_reset("p2")
			p2_reg_turn = 0

func end_turn():
	emit_signal("turn_done")
	yield()

#these function will contain logic for "passive" effects(when info needs to be continuously stored)
func receive_p1_spell(spell_number):
	p1_spell = numb_to_node(spell_number, "p1")
func receive_p2_spell(spell_number):
	p2_spell = numb_to_node(spell_number, "p2")
	
func numb_to_node(sn, player): #passive effects are activated here
	if sn == 1:
		return get_node("Royal Banquet")
	elif sn == 2:
		return get_node("Premonition")
	elif sn == 3:
		get_node("Resurrection").pre_effect(player)
		return get_node("Resurrection")
	elif sn == 4:
		return get_node("Soul Swap")
	elif sn == 5:
		return get_node("Regicide")
	elif sn == 6:
		return get_node("Field of Blood")


func reset():
	active_player = null
	turn_count = 0

	p1_used = false
	p2_used = false

	p1_res_turn = 0
	p2_res_turn = 0
	p1_reg_turn = 0
	p2_reg_turn = 0
