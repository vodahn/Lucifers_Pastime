extends Node
#turn structure/allowances 
# this will contain things like adding no summoning zones and removing them, 
#which movement and no summoning zones will be called when necessary
#gui elements and notiation input will call upon this script
var active_player = self.get_child(0)
var turn_count = 0 

signal turn_pass(player)
signal turn_update
signal send_player(player)

func _ready():
	for i in get_children():
		self.connect("turn_pass", i, "on_turn")
	emit_signal("turn_pass", active_player)

func change_player():
	var new_index : int = (active_player.get_index() + 1) % get_child_count()
	active_player = get_child(new_index)
	emit_signal("turn_pass", active_player)

func turn_count_increase():
	if turn_count > 0:
		var queen_check = false
		for i in get_node("p1/p1 container").get_children():
			if i.piece_type == "Queen":
				queen_check = true
				break
		if queen_check == false:
			print("player 2 wins")
			won_game("p2")
			return
		elif turn_count > 1:
			var queen_check2 = false
			for i in get_node("p2/p2 container").get_children():
				if i.piece_type == "Queen":
					queen_check2 = true
					break
			if queen_check2 == false:
				print("player 1 wins")
				won_game("p1")
				return
	turn_count += 1
	emit_signal("field_update")
	emit_signal("turn_update")

func send_p1():
	if turn_count % 2:
		emit_signal("send_player", get_node("p1"))
func send_p2():
	if turn_count % 2 == 0:
		emit_signal("send_player", get_node("p2"))

func mp_display():
	get_parent().get_node("p1 mp").text = str(get_node("p1/p1mp").mp)
	get_parent().get_node("p2 mp").text = str(get_node("p2/p2mp").mp)

func won_game(player):
	if player == "p1":
		get_parent().get_node("Notation Container/Notation Box").bbcode_text += ("\n\n" + 
					"Player 1 Wins" + "\t" + "(＾◇＾)")
	if player == "p2":
		get_parent().get_node("Notation Container/Notation Box").bbcode_text += ("\n\n" + 
				"Player 2 Wins" + "\t" + "(＾◇＾)")
	if player == "p1f":
		get_parent().get_node("Notation Container/Notation Box").bbcode_text += ("\n\n" + 
					"[url]︵ ┻━┻[/url]" + "\n\n" +
					"Player 1 Wins" + "\t" + "(＾◇＾)")
	if player == "p2f":
		get_parent().get_node("Notation Container/Notation Box").bbcode_text += ("\n\n" + 
					"[url]︵ ┻━┻[/url]" + "\n\n" +
					"Player 2 Wins" + "\t" + "(＾◇＾)")
	post_game()

func post_game():
	Movement.block_move()
	Summoning.post_game()
	get_parent().get_node("Board").disable_board()
	get_parent().get_node("Notation Container/Input").disable_input()
	get_parent().get_node("Options/Options Container").turn_count = 0
	
	get_parent().get_node("Options/Options Container/Deck Container/Deck").set_disabled(false)
	get_parent().get_node("Options/Options Container/Deck Container/Deck").mouse_filter = 1
	get_parent().get_node("Notation Container/Input").won()
	turn_count = 0

func reset():
	active_player = self.get_child(0)
	emit_signal("turn_pass", active_player)
	turn_count = 0
	#empty piece containers and reset piece type ammount
	for i in get_node("p1/p1 container").get_children():
		Release.release(i, "p1")
		i.queue_free()
	for i in get_node("p2/p2 container").get_children():
		Release.release(i, "p2")
		i.queue_free()
	Queen.p1_amm = 0
	Queen.p2_amm = 0
	get_node("p1/p1mp").mp = 200
	get_node("p2/p2mp").mp = 200
	get_parent().get_node("p1 mp").text = "200"
	get_parent().get_node("p2 mp").text = "200"

	#resetting piecetype numbers for id
	Apprentice.p1_number = ["p1", 0]
	IronMaiden.p1_number = ["p1", 0]
	Nekomata.p1_number = ["p1", 0]
	IttanMomen.p1_number = ["p1", 0]
	Harpy.p1_number = ["p1", 0]
	Slime.p1_number = ["p1", 0]
	Redcap.p1_number = ["p1", 0]
	Holstaur.p1_number = ["p1", 0]
	RedOni.p1_number = ["p1", 0]
	BlueOni.p1_number = ["p1", 0]
	Priestess.p1_number = ["p1", 0]
	Imp.p1_number = ["p1", 0]
	FalseAngel.p1_number = ["p1", 0]
	QueenSlime.p1_number = ["p1", 0]
	Automaton.p1_number = ["p1", 0]
	Sylph.p1_number = ["p1", 0]
	Queen.p1_number = ["p1", 0]
	
	Apprentice.p2_number = ["p2", 0]
	IronMaiden.p2_number = ["p2", 0]
	Nekomata.p2_number = ["p2", 0]
	IttanMomen.p2_number = ["p2", 0]
	Harpy.p2_number = ["p2", 0]
	Slime.p2_number = ["p2", 0]
	Redcap.p2_number = ["p2", 0]
	Holstaur.p2_number = ["p2", 0]
	RedOni.p2_number = ["p2", 0]
	BlueOni.p2_number = ["p2", 0]
	Priestess.p2_number = ["p2", 0]
	Imp.p2_number = ["p2", 0]
	FalseAngel.p2_number = ["p2", 0]
	QueenSlime.p2_number = ["p2", 0]
	Automaton.p2_number = ["p2", 0]
	Sylph.p2_number = ["p2", 0]
	Queen.p2_number = ["p2", 0]
